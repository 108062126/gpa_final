#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

in VertexData
{
    vec3 N; // eye space normal
    vec3 L; // eye space light vector
    vec3 H; // eye space halfway vector
    vec2 texcoord;
	vec4 shadowTc;
} vertexData;

uniform struct{
	vec3 Ka;
	vec3 Kd;
	vec3 Ks;
}lightParam;

uniform sampler2D dfTex;
uniform sampler2D amTex;
uniform sampler2D spTex;
uniform sampler2D normalTex;
uniform sampler2DShadow shadowTex;
uniform float lightStr;

in mat3 fragTBN;

void main()
{
	vec3 normal = texture(normalTex, vertexData.texcoord).rgb;
	normal = normalize(normal * 2.0 - 1.0);

	float shadowFactor = textureProj(shadowTex, vertexData.shadowTc);
	shadowFactor = clamp(shadowFactor, 0.3, 1.0);

	vec3 Intensity = vec3(0.0, 0.0, 0.0);
	float cKd, cKs, d, attenuation;

	//d = distance(vertexData.L, vec3(0.0, 0.0, 0.0));
	//attenuation = min(1/(0.01 + d*0.000003 + d*d*0.00001), 1);

	//ambient
	vec4 amColor = texture(amTex, vertexData.texcoord);
	Intensity += amColor.rgb * lightParam.Ka;

	//diffuse
	vec4 dfColor = texture(dfTex, vertexData.texcoord);
	if(dfColor.a == 0)discard;
	cKd = max(0, dot(normalize(vertexData.L), normalize(normal)));
	Intensity += lightStr * dfColor.rgb * cKd * lightParam.Kd * 1.2;

	//specular
	vec4 spColor = texture(spTex, vertexData.texcoord);
	cKs = max(0, pow(dot(normalize(vertexData.H), normalize(normal)), 32));
	Intensity += spColor.rgb * cKs * lightParam.Ks;

	normalColor = vec4(normalize(fragTBN * normal) * 0.5 + 0.5, 1.0);
	fragColor = vec4(Intensity * shadowFactor, 1.0);
}