#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

in vec2 texCoord;
in vec4 shadowTc;
in vec3 fragNormal;
in vec2 tc;
uniform sampler2D dfTex;
uniform sampler2D heightMap;
uniform sampler2DShadow shadowTex;
uniform float lightStr;

void main()
{
	float sFactor = textureProj(shadowTex, shadowTc);
	sFactor = clamp(sFactor, 0.3, 1.0);
	vec4 height = texture(heightMap, tc);
	vec4 color = texture(dfTex, texCoord);
	if(color.a <= 0.5)discard;
	if(height.y <= 0.4)discard;
	color.rgb = color.rgb * lightStr;
	fragColor = color * vec4(vec3(sFactor), 1.0);
	normalColor = vec4(normalize(fragNormal) * 0.5 + 0.5, 1.0);
}