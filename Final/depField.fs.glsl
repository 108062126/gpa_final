#version 410

layout(location = 0) out vec4 fragColor;

in vec2 texcoord;

uniform sampler2D tex;
uniform sampler2D blurTex;
uniform sampler2D viewDepTex;
uniform sampler2D ssaoTex;
uniform int ssaoEn;

void main(){

	if(texcoord.x > 0.498 && texcoord.x < 0.502 && texcoord.y > 0.497 && texcoord.y < 0.503)
		fragColor = vec4(0.3, 0.7, 1.0, 1.0);
	else {
		float ssao = texture(ssaoTex, texcoord).r;
		float centerDep = texture(viewDepTex, vec2(0.5, 0.5)).r;
		float pointDep = texture(viewDepTex, texcoord).r;
		vec4 colorBlur = texture(blurTex, texcoord);
		vec4 colorClear = texture(tex, texcoord);

		float nearRange = 0.05;
		float farRange = 0.08;
		float nearStart = 0.005;
		float farStart = 0.01;

		float depthVal =  -pointDep + centerDep;
		float coc = 0.0;

		if(depthVal < 0.0){
			coc = (-depthVal - nearStart) / nearRange;
		 }
		else{
			coc = (depthVal - farStart) / farRange;
		}
		coc = clamp(coc, 0.0, 1.0);
		if(ssaoEn == 0)ssao = 1.0;
		fragColor = vec4(mix(colorClear, colorBlur, coc).rgb * ssao, 1.0);
	}
}