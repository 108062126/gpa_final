#version 410

layout(location = 0) out vec4 fragColor;

in vec2 texcoord;

uniform sampler2D tex;
uniform sampler2D noise_tex;
uniform sampler2D ssaoTex;

uniform vec2 img_sz;
uniform int en;
uniform int ssaoEn;

void main(){
	vec4 color;
	if(en != 2){
		float ssao = texture(ssaoTex, texcoord).r;
		if(ssaoEn == 0)ssao = 1.0;
		fragColor = vec4(texture(tex, texcoord).rgb * ssao, 1.0);
	}
	else 
	{
		fragColor = vec4(0.0f);
		int n = 0;
		int half_sz = 2;
		vec2 offset = texture(noise_tex, texcoord).rg / 100.0f;
		for(int i = -1 * half_sz; i <= half_sz; i++)
		{
			for(int j = -1 * half_sz; j <= half_sz; j++)
			{
				vec2 new_texcoord = texcoord + offset + ivec2(i, j)/img_sz;
				//clamp(new_texcoord.x, 0.01f, 0.99f);
				new_texcoord.y = clamp(new_texcoord.y, 0.001f, 0.999f);
				fragColor += vec4(texture(tex, new_texcoord).rgb, 1.0f);
				n++;
			}
		}
		fragColor /= n;

		float nbins = 8.0f;
		vec3 tmp = fragColor.rgb;
		tmp = floor(tmp * nbins) / nbins;
		float ssao = texture(ssaoTex, texcoord).r;
		if(ssaoEn == 0)ssao = 1.0;
		fragColor = vec4(tmp * ssao, 1.0f);
	}
}