#version 410

layout(location = 0) out vec4 fragColor;

in vec2 texcoord;

uniform sampler2D tex;

void main(){
	fragColor = vec4(texture(tex, texcoord).rgb, 1.0);
}