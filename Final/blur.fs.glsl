#version 410

layout(location = 0) out vec4 fragColor;

in vec2 texcoord;

uniform sampler2D tex;
uniform vec2 img_size;

void main(){
	vec3 color = vec3(0.0, 0.0, 0.0);
	int m = 0;
	int half_size = 5;
	for(int i = -half_size; i <= half_size; i++){
		for(int j = -half_size; j <= half_size; j++){
			vec4 t = texture(tex, texcoord + vec2(i, j) / img_size);
			color += t.rgb;
			m++;
		}
	}
	color /= float(m);
	fragColor = vec4(color, 1.0);
}