#version 410

layout(location = 0) in vec3 iv3vertex;

uniform mat4 um4vp;

void main()
{
	gl_Position = um4vp * vec4(iv3vertex, 1.0);
}