#version 410

layout(location = 0) out vec4 fragColor;

in vec3 texDir;
uniform samplerCube cubemap;
uniform float lightStr;

void main(){
	fragColor = vec4(texture(cubemap, texDir).rgb * lightStr, 1.0);
}