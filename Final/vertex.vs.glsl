#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec2 iv2tex_coord;
layout(location = 2) in vec3 iv3normal;
layout(location = 3) in vec3 iv3tangent;
layout(location = 4) in vec3 iv3bitangent;

uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform mat4 shadowMat;
uniform vec3 camPos;

uniform vec4 plane;

out VertexData
{
    vec3 N; // eye space normal
    vec3 L; // eye space light vector
    vec3 H; // eye space halfway vector
    vec2 texcoord;
	vec4 shadowTc;
} vertexData;

out mat3 fragTBN;

void main()
{
	vec3 normal = (transpose(inverse(um4m)) * vec4(iv3normal, 0.0)).xyz;
	vec3 tangent = (transpose(inverse(um4m)) * vec4(iv3tangent, 0.0)).xyz;
	vec3 bitangent = (transpose(inverse(um4m)) * vec4(iv3bitangent, 0.0)).xyz;

	mat3 TBN = transpose(mat3(normalize(tangent), normalize(bitangent), normalize(normal)));

	vec4 pos = um4m * vec4(iv3vertex, 1.0);

	gl_ClipDistance[0] = dot(pos, plane);

	gl_Position = um4p * um4v * pos;
    vertexData.texcoord = iv2tex_coord;
	vertexData.N = normal;
	vertexData.L = TBN * vec3(-4.0, 5.5, 2.5);
	vertexData.H = TBN * (normalize(vec3(-4.0, 5.5, 2.5)) + normalize(camPos - pos.xyz));
	vertexData.shadowTc = shadowMat * pos;
	fragTBN = transpose(TBN);
}