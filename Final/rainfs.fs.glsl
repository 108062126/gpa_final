#version 410

out vec4 fragColor;

in vec2 viewCoord;

void main(){
	
	float d = length(viewCoord);
	if(d >= 15.0)discard;

	 fragColor = vec4(0.2, 0.5, 1.0, 1.0);
}