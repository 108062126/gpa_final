#version 410

uniform float level;

out VS_OUT{
	vec2 tc;
}vs_out;

void main(){
	const vec4 vertices[] = vec4[](
		vec4(-1.0, 0.0, -1.0, 1.0),
		vec4(1.0, 0.0, -1.0, 1.0),
		vec4(-1.0, 0.0, 1.0, 1.0),
		vec4(1.0, 0.0, 1.0, 1.0)
	);

	int x = gl_InstanceID & 63;
	int y = gl_InstanceID >> 6;
	vec2 offset = vec2(x, y);

	vs_out.tc = (vertices[gl_VertexID].xz + offset) / 64.0;
	gl_Position = vertices[gl_VertexID] + vec4(float(offset.x - 32), 0.0, float(offset.y - 32), 0.0);
}