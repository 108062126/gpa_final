#version 410

layout (triangles, invocations = 1) in;
layout (line_strip, max_vertices = 3) out;

void main(){
	int i;
	for(i = 0; i < gl_in.length(); i++){
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}
}
