#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec2 iv2tex_coord;
layout(location = 2) in vec3 iv3normal;

uniform mat4 shadow_matrix;
uniform mat4 um4m;
uniform mat4 trans_inv_m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform vec3 camera_pos;

out VertexData
{
    vec3 N2; 
    vec3 L;
    vec3 V;
    vec3 H;

    vec3 N; // eye space normal
    vec3 view;
    vec2 texcoord;
    vec4 shadowcoord;
} vertexData;

vec3 light_pos = vec3(-40, 55.0, 25.0);

void main()
{
    vertexData.texcoord = iv2tex_coord;

    vec4 pos = um4m * vec4(iv3vertex, 1.0);
    vertexData.shadowcoord = shadow_matrix * pos;

    // blinn
    vertexData.N2 = normalize( mat3(um4m) * iv3normal);
    vertexData.L = normalize(light_pos - pos.xyz);
    vertexData.V = normalize(-pos.xyz);
    vertexData.H = normalize(vertexData.L + vertexData.V);

    // env map
    vertexData.view = pos.xyz - camera_pos;
    vertexData.N = mat3(trans_inv_m) * iv3normal;
	
    gl_Position = um4p * um4v * pos;
}