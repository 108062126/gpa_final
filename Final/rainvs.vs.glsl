#version 410

layout(location = 0) in vec3 iv3vertex;
uniform mat4 mv;
uniform mat4 p;
uniform vec3 velocity;
uniform sampler2D noise;

out vec2 viewCoord;

float random(){
	return (gl_InstanceID % 7 + gl_InstanceID * gl_InstanceID * 53 % 4);
}

void main(){
	int x = gl_InstanceID & 63;
	int y = gl_InstanceID >> 6;
	vec2 off = vec2(x, y);
	vec2 tc = ((iv3vertex.xz + off) / 64.0);

	vec2 distor1 = texture(noise, tc).rg * 0.1;
	distor1 = tc + vec2(distor1.x, distor1.y + random() / 11.0);
	vec3 displace = texture(noise, vec2(distor1.x + random() / 11.0, distor1.y)).rgb * 2.0 - 1.0;
	displace.y = pow(displace.y, 32);
	float height;
	if(velocity.y * -1 > random() * 15){
		float v = velocity.y + random() * 15;
		height = random() * 15.0 + v;
	}
	else{
		height = random() * 15.0 + velocity.y;
	}
	vec3 offset = vec3(x -32, height + displace.r, y - 32) ;
	vec4 pos = mv * vec4(iv3vertex + offset, 1.0);
	viewCoord = pos.xz;
	gl_Position = p * pos;
}