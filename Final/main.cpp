#include "../Externals/Include/Common.h"

#define MENU_TIMER_START 1
#define MENU_TIMER_STOP 2
#define MENU_EXIT 3
#define MENU_SHOW_DEPTH_OF_VIEW 4
#define MENU_SHOW_WATERCOLOR 5
#define MENU_NO_EFFECT 6

GLfloat timer_cnt = 0;
bool timer_enabled = true;
unsigned int timer_speed = 16;
bool ssaoFlag = false;

using namespace glm;
using namespace std;
using namespace tinyobj;

//add
GLuint programWaterColor, programSword, programSphere;
GLuint noise_map, Sting_Base_Color;
int effect = 0;

int rainflag;

GLuint programCube;

GLuint programDepMap;
GLuint depthFBO;
GLuint shadowDepTex;
GLuint depthVpLoc;

GLuint cubeTex;
GLuint cubeTexLoc;
GLuint cubeVAO, cubeVBO;
GLuint cubemv, cubep;
GLuint cubeLightStrLoc;

GLuint quadFlagLoc;
GLuint quadVAO, quadVBO;

mat4 proj(1.0f);
mat4 view(1.0f);
mat4 orthLeft(1.0f);
mat4 orthRight(1.0f);
mat4 lightProj(1.0f);
mat4 lightView(1.0f);
mat4 shadowBias(1.0f);
mat4 shadowSbpv(1.0f);
bool mouseDrag = false;
float off_x, off_y;
float rainedLS = 0.5;

void renderTerrain(vec4);
void renderModels(vec4);
void renderCubeBox();
void renderGrass();

int windowWidth, windowHeight;

vec3 ptPos = vec3(29.1675, 6.5, 11.5157);
vector<float> ptsPos;

struct modelRenderer {
	GLuint program;
	GLuint um4m, um4v, um4p;
	GLuint camPosLoc;
	GLuint kaLoc, kdLoc, ksLoc;
	GLuint amTexLoc, dfTexLoc, spTexLoc, normalTexLoc;
	GLuint shadowMatLoc, shadowTexLoc;
	GLuint envMapLoc;
	GLuint clipPlaneLoc;
	GLuint lightStrLoc;
}modelRenderer;

struct Quad {
	GLuint program;
	GLuint um4m, um4v, um4p;
	GLuint tcsMvpLoc;
	GLuint clipPlaneLoc;
	GLuint shadowMatLoc, shadowTexLoc;
	GLuint camPosLoc;
	GLuint dudvMapLoc, moveFactorLoc, waterNormalLoc;
	GLuint lightStrLoc;
}quad;

struct Grass {
	GLuint program;
	GLuint noiseTex, noiseTexLoc;
	GLuint dfTex, dfTexLoc;
	GLuint heightMapLoc;
	GLuint moveFactorLoc;
	GLuint mvpLoc;
	GLuint shadowMatLoc;
	GLuint shadowTexLoc;
	GLuint lightStrLoc;
	float moveFactor;
}grass;

struct Terrain {
	GLuint heightMap;
	GLuint normalMap;
	GLuint texLoc, normalLoc;
	GLuint landTex, landTexLoc;
}terrain;

struct window {
	GLuint program;
	GLuint VAO, VBO;
	GLuint fboTexLoc;
	GLuint texFBO;
	GLuint FBO;
}window;

struct depField {
	GLuint programBlur, program;
	GLuint texBlur, viewDepTex;
	GLuint blurTexLoc, texLoc, viewDepTexLoc;
	GLuint imgSizeLoc, texLoc_for_blur;
	GLuint ssaoTexLoc;
}depField;

class depth {
public:
	GLuint programTerrain, programModel;
	GLuint terrainHeightMapLoc;
	GLuint terrainUm4m, terrainUm4v, terrainUm4p;
	GLuint modelUm4m, modelUm4v, modelUm4p;
	GLuint terrainClipLoc, modelClipLoc;
	GLuint terrainTCSMvpLoc;
	GLuint modelTexLoc;
}depth;

struct ssao {
	GLuint program;
	GLuint normal_mapLoc;
	GLuint depth_mapLoc;
	GLuint noise_mapLoc;
	GLuint noise_scaleLoc;
	GLint blockIdx;
	GLuint vao;
	GLuint projLoc;
	GLuint kernal_ubo;
	GLuint noise_map;
	GLuint tex;
}ssao;

struct cameraLine {
	GLuint program;
	GLuint VAO, VBO;
	GLuint vaoLine, vboLine;
	GLuint mvpLoc;
	int numPts;
}cameraLine;

struct
{
	GLuint normal_map;
} gbuffer;

float skyboxVertices[] = {
	// positions          
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f
};

float quadVertices[] = {
	-1.0, -0.5, -1.0,
	1.0, -0.5, -1.0,
	-1.0, -0.5, 1.0,
	1.0, -0.5, 1.0
};

vec3 pts[] = {
vec3(29.1675, 6.5, 11.5157),
vec3(28.2024, 6.2, 25.2449),
vec3(16.5605, 5.9, 33.1725),
vec3(3.66935, 5.6, 34.8513),
vec3(-12.2947, 5.3, 33.5455),
vec3(-26.191, 5, 28.6886),
vec3(-33.8458, 5, 20.896),
vec3(-35.3958, 5, 12.7917),
vec3(-34.0838, 5, 7.86435),
vec3(-31.2633, 4.7, 3.73583),
vec3(-27.4893, 4.1, 1.69503),
vec3(-21.8206, 3.5, 0.938593),
vec3(-15.5075, 3.2, 5.9798),
vec3(-12.2174, 2.6, 12.5413),
vec3(-10.5387, 2, 19.3291),
vec3(-13.9167, 0.799999, 24.2252),
vec3(-19.4978, -0.400001, 28.628),
vec3(-23.6566, -0.700001, 29.3592),
vec3(-27.7738, -0.700001, 27.2718),
vec3(-28.4202, -0.700001, 21.4768),
vec3(-28.78, -0.100001, 15.2434),
vec3(-28.256, 0.799999, 6.59905),
vec3(-22.9419, 1.4, -3.51782),
vec3(-14.7159, 2, -9.29228),
vec3(-4.52012, 2.6, -8.82401),
vec3(3.4239, 3.2, -5.0476),
vec3(8.19109, 3.8, 2.41019),
vec3(7.37971, 4.7, 10.049),
vec3(2.12326, 4.1, 16.4058),
vec3(-6.4, 2.45, 19.4),
vec3(-9.73495, 1.4, 17.9682)
};

vec3 viewCenterPts[] = {
	vec3(28.1704, 7.4, 11.439),
	vec3(23.8811, 6.2, 20.7548),
	vec3(15.4556, 5.6, 25.8419),
	vec3(3.78797, 5.6, 27.2919),
	vec3(-9.69166, 5.6, 24.9623),
	vec3(-21.8169, 3.8, 22.2534),
	vec3(-25.8304, 3.2, 18.9088),
	vec3(-27.6724, 2.9, 13.8485),
	vec3(-27.4109, 2.9, 10.8599),
	vec3(-26.8812, 2.9, 9.65608),
	vec3(-23.4941, 1.7, 9.09281),

	vec3(-23.3207, 1.2, 9.92562),
	vec3(-21.727, -0.799999, 14.6358),
	vec3(-20.6592, -1.199999, 16.5008),
	vec3(-20.1587, -1.500001, 18.6802),
	vec3(-21.8058, -1.700001, 21.1876),
	vec3(-23.4155, -2.0, 21.6604),
	vec3(-23.6645, -2.3, 20.2695),
	vec3(-25.0352, -1.100001, 16.2252),
	vec3(-23.3673, 0.499999, 9.10211),
	vec3(-18.2461, 1.7, 1.01201),
	vec3(-12.909, 2.6, -2.90399),
	vec3(-5.71954, 4.1, -3.65109),
	vec3(-0.633226, 4.7, -0.16372),
	vec3(-0.430606, 5.3, 2.88315),
	vec3(-0.765517, 6.2, 5.78367),
	vec3(-1.6715, 6.2, 5.75535),
	vec3(-2.41233, 6.2, 5.31576),
	vec3(-4.30103, 6.2, 4.11875),

};

vec3 treeInstT[10] = { vec3(0.0, 0.6, 0.0), vec3(18.0, 1.0, 0.0), vec3(5.0, 0.0, 18.0), vec3(-17.0, 1.0, 0.0), vec3(-5.0, 1.0, -23.0) };
vec3 treeInstR[10] = { vec3(0.0, 0.0, 0.0), vec3(0.0, 0.5, 0.0), vec3(0.0, 1.3, 0.0), vec3(0.0, 0.2, 0.0), vec3(0.0, 2.3, 0.0) };
vec3 treeInstS[10] = { vec3(0.1, 0.07, 0.1), vec3(0.01, 0.01, 0.01), vec3(0.016, 0.016, 0.016), vec3(0.02, 0.02, 0.017), vec3(0.02, 0.02, 0.02) };


struct rain {
	GLuint particleTexture;
	GLuint renderProgram;
	GLuint vao;
	GLuint velocityLoc;
	GLuint mvLoc, pLoc;
	GLuint buffer;
	GLuint noiseLoc;
	vec3 velocity;
}rain;

static const GLfloat raindrop[] = {
	-0.01f, 150.0f, 0.01f,
	-0.01f,148.5f, -0.01f,
	0.01f, 148.5f, 0.0f,

	0.01f, 148.5f, 0.01f,
	0.01f, 150.0f, -0.01f,
	-0.01f, 150.0f, 0.0f,
};


static const GLfloat windowPos[] = {

		-1.0f, 1.0f, 0.0f, //up left
		0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, //down left
		0.0f, 0.0f,
		1.0f, 1.0f, 0.0f, //up right
		1.0f, 1.0f,


		1.0f, 1.0f, 0.0f, //up right
		1.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, //down left
		0.0f, 0.0f,
		1.0f, -1.0f, 0.0f, //down right
		1.0f, 0.0f

};

static const char cubeMaps[][30] = { "cubemaps/right.jpg", "cubemaps/left.jpg", "cubemaps/top.jpg", "cubemaps/bottom.jpg", "cubemaps/back.jpg", "cubemaps/front.jpg" };

struct camera {
	vec3 position;
	vec3 viewVec;
	vec3 upVec;
	float moveFactor, moveSpeed;
}camera;

typedef struct shape {
	GLuint vao;
	GLuint vbo_position;
	GLuint vbo_normal;
	GLuint vbo_texcoord;
	GLuint vbo_tangent;
	GLuint ebo;
	int drawCount;
	int materialID;
}Shape;
//add
Shape Sword, Sphere;

typedef struct material {
	vec3 Ka;
	vec3 Kd;
	vec3 Ks;
	GLuint diffuse_tex;
	GLuint ambient_tex;
	GLuint spec_tex;
	GLuint normal_tex;
}Material;

typedef struct model {
	vector<Shape> shapes;
	vector<Material> materials;
	vec3 t;
	vec3 r;
	vec3 s;
}model;

vector<model> models;

class Water {
public:
	GLuint program;
	GLuint FBO, RBO;
	GLuint texRFR, texRFL;
	GLuint texRFRLoc, texRFLLoc;
	GLuint um4v, um4p;
	GLuint levelLoc;
	GLuint texDUDV;
	GLuint dudvLoc;
	GLuint moveFactorLoc;
	GLuint normalMap, normalMapLoc;
	GLuint camPosLoc;
	GLuint disMap, disMapLoc;
	GLuint tcsMvpLoc;
	GLuint shadowTexLoc, shadowMatLoc;
	GLuint lightStrLoc;
	void reshape(int width, int height) {
		glDeleteRenderbuffers(1, &RBO);
		glGenRenderbuffers(1, &RBO);
		glBindRenderbuffer(GL_RENDERBUFFER, RBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);

		glDeleteTextures(1, &texRFR);
		glDeleteTextures(1, &texRFL);

		glGenTextures(1, &texRFR);
		glBindTexture(GL_TEXTURE_2D, texRFR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glGenTextures(1, &texRFL);
		glBindTexture(GL_TEXTURE_2D, texRFL);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FBO);
		glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, RBO);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texRFR, 0);
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texRFL, 0);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	}
	void renderRFR_RFL() {
		glBindFramebuffer(GL_FRAMEBUFFER, FBO);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glEnable(GL_CLIP_DISTANCE0);
		//RFR
		renderTerrain(vec4(0.0, -1.0, 0.0, water.level));
		renderModels(vec4(0.0, -1.0, 0.0, water.level));

		//RFL
		glDrawBuffer(GL_COLOR_ATTACHMENT1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		float d = 2 * (camera.position.y - water.level);
		camera.viewVec.y *= -1;
		camera.position += vec3(0.0, -d, 0.0);
		camera.upVec *= -1;
		view = lookAt(camera.position, camera.position + camera.viewVec, camera.upVec);

		renderCubeBox();
		renderTerrain(vec4(0.0, 1.0, 0.0, -1 * water.level));
		renderModels(vec4(0.0, 1.0, 0.0, -1 * water.level));
		renderGrass();

		camera.viewVec.y *= -1;
		camera.position += vec3(0.0, d, 0.0);
		camera.upVec *= -1;
		view = lookAt(camera.position, camera.position + camera.viewVec, camera.upVec);

		glDisable(GL_CLIP_DISTANCE0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glBindVertexArray(0);
		glUseProgram(0);
	}
	float level, moveFactor;
}water;

void renderWater() {
	glUseProgram(water.program);
	glBindVertexArray(quadVAO);
	glUniformMatrix4fv(water.um4v, 1, GL_FALSE, value_ptr(view));
	glUniformMatrix4fv(water.um4p, 1, GL_FALSE, value_ptr(proj));
	glUniformMatrix4fv(water.tcsMvpLoc, 1, GL_FALSE, value_ptr(proj * view));
	glUniformMatrix4fv(water.shadowMatLoc, 1, GL_FALSE, value_ptr(shadowSbpv));
	glUniform1f(water.levelLoc, water.level);
	glUniform1f(water.moveFactorLoc, water.moveFactor);
	glUniform1f(water.lightStrLoc, rainflag ? rainedLS : 1.0);
	glUniform3fv(water.camPosLoc, 1, value_ptr(camera.position));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, water.texRFR);
	glUniform1i(water.texRFRLoc, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, water.texRFL);
	glUniform1i(water.texRFLLoc, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, water.texDUDV);
	glUniform1i(water.dudvLoc, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, water.normalMap);
	glUniform1i(water.normalMapLoc, 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, water.disMap);
	glUniform1i(water.disMapLoc, 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, shadowDepTex);
	glUniform1i(water.shadowTexLoc, 5);

	glDrawArraysInstanced(GL_PATCHES, 0, 4, 64 * 64);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderTexture(GLuint tex) {
	glUseProgram(window.program);
	glBindVertexArray(window.VAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(window.fboTexLoc, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderBlurTexture(GLuint tex) {
	glUseProgram(depField.programBlur);
	glBindVertexArray(window.VAO);

	glUniform2fv(depField.imgSizeLoc, 1, value_ptr(vec2((float)windowWidth, (float)windowHeight)));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);
	glUniform1i(depField.texLoc_for_blur, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderDepFieldTexture() {
	glUseProgram(depField.program);
	glBindVertexArray(window.VAO);

	glUniform1i(glGetUniformLocation(depField.program, "ssaoEn"), ssaoFlag ? 1 : 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, window.texFBO);
	glUniform1i(depField.texLoc, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depField.texBlur);
	glUniform1i(depField.blurTexLoc, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, depField.viewDepTex);
	glUniform1i(depField.viewDepTexLoc, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, ssao.tex);
	glUniform1i(depField.ssaoTexLoc, 3);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderTerrain(vec4 plane) {

	glUseProgram(quad.program);
	glUniformMatrix4fv(quad.um4m, 1, GL_FALSE, value_ptr(mat4(1.0)));
	glUniformMatrix4fv(quad.um4v, 1, GL_FALSE, value_ptr(view));
	glUniformMatrix4fv(quad.um4p, 1, GL_FALSE, value_ptr(proj));
	glUniformMatrix4fv(quad.tcsMvpLoc, 1, GL_FALSE, value_ptr(proj * view));
	glUniformMatrix4fv(quad.shadowMatLoc, 1, GL_FALSE, value_ptr(shadowSbpv));
	glUniform3fv(quad.camPosLoc, 1, value_ptr(camera.position));
	glUniform4fv(quad.clipPlaneLoc, 1, value_ptr(plane));
	glUniform1f(quad.moveFactorLoc, water.moveFactor);
	glUniform1f(quad.lightStrLoc, rainflag ? rainedLS : 1.0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, terrain.heightMap);
	glUniform1i(terrain.texLoc, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, terrain.normalMap);
	glUniform1i(terrain.normalLoc, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, terrain.landTex);
	glUniform1i(terrain.landTexLoc, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadowDepTex);
	glUniform1i(quad.shadowTexLoc, 3);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, water.texDUDV);
	glUniform1i(quad.dudvMapLoc, 4);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, water.normalMap);
	glUniform1i(quad.waterNormalLoc, 5);

	glBindVertexArray(quadVAO);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawArraysInstanced(GL_PATCHES, 0, 4, 64 * 64);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderTerrainDep(vec4 plane) {

	glUseProgram(depth.programTerrain);
	glUniformMatrix4fv(depth.terrainUm4m, 1, GL_FALSE, value_ptr(mat4(1.0)));
	glUniformMatrix4fv(depth.terrainUm4v, 1, GL_FALSE, value_ptr(view));
	glUniformMatrix4fv(depth.terrainUm4p, 1, GL_FALSE, value_ptr(proj));
	glUniformMatrix4fv(depth.terrainTCSMvpLoc, 1, GL_FALSE, value_ptr(proj * view));
	glUniform4fv(depth.terrainClipLoc, 1, value_ptr(plane));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, terrain.heightMap);
	glUniform1i(depth.terrainHeightMapLoc, 0);

	glBindVertexArray(quadVAO);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawArraysInstanced(GL_PATCHES, 0, 4, 64 * 64);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderShadowMap();

void renderModels(vec4 plane) {

	glUseProgram(modelRenderer.program);
	glUniform4fv(modelRenderer.clipPlaneLoc, 1, value_ptr(plane));
	mat4 r, s, t;
	for (int j = 0; j < 5; j++) {
		for (auto model : models) {
			model.r = treeInstR[j];
			model.s = treeInstS[j];
			model.t = treeInstT[j];
			r = rotate(mat4(1.0), model.r.x, vec3(1.0, 0.0, 0.0));
			r *= rotate(mat4(1.0), model.r.y, vec3(0.0, 1.0, 0.0));
			r *= rotate(mat4(1.0), model.r.z, vec3(0.0, 0.0, 1.0));
			s = scale(mat4(1.0), model.s);
			t = translate(mat4(1.0), model.t);

			glUniformMatrix4fv(modelRenderer.um4m, 1, GL_FALSE, value_ptr(t * r * s));
			glUniformMatrix4fv(modelRenderer.um4v, 1, GL_FALSE, value_ptr(view));
			glUniformMatrix4fv(modelRenderer.um4p, 1, GL_FALSE, value_ptr(proj));
			glUniformMatrix4fv(modelRenderer.shadowMatLoc, 1, GL_FALSE, value_ptr(shadowSbpv));
			glUniform1f(modelRenderer.lightStrLoc, rainflag ? rainedLS : 1.0);
			glUniform3fv(modelRenderer.camPosLoc, 1, value_ptr(camera.position));

			for (int i = 0; i < model.shapes.size(); i++) {
				int m = model.shapes[i].materialID;
				glBindVertexArray(model.shapes[i].vao);

				glUniform1i(modelRenderer.amTexLoc, 0);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, model.materials[m].ambient_tex);

				glUniform1i(modelRenderer.dfTexLoc, 1);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, model.materials[m].diffuse_tex);

				glUniform1i(modelRenderer.spTexLoc, 2);
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, model.materials[m].spec_tex);

				glUniform1i(modelRenderer.normalTexLoc, 3);
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, model.materials[m].normal_tex);

				glUniform1i(modelRenderer.shadowTexLoc, 4);
				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, shadowDepTex);

				glUniform3fv(modelRenderer.kaLoc, 1, value_ptr(model.materials[m].Ka));
				glUniform3fv(modelRenderer.kdLoc, 1, value_ptr(model.materials[m].Kd));
				glUniform3fv(modelRenderer.ksLoc, 1, rainflag ? value_ptr(vec3(0.8, 0.8, 0.8)) : value_ptr(model.materials[m].Ks));

				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
				glEnableVertexAttribArray(2);
				glEnableVertexAttribArray(3);
				glEnableVertexAttribArray(4);

				glDrawElements(GL_TRIANGLES, model.shapes[i].drawCount, GL_UNSIGNED_INT, 0);
				glBindVertexArray(0);
			}
		}
	}
	glUseProgram(0);
}

void renderModelsDep(vec4 plane) {

	glUseProgram(depth.programModel);
	glUniform4fv(depth.modelClipLoc, 1, value_ptr(plane));
	mat4 r, s, t;
	for (int j = 0; j < 5; j++) {
		for (auto model : models) {
			model.r = treeInstR[j];
			model.s = treeInstS[j];
			model.t = treeInstT[j];
			r = rotate(mat4(1.0), model.r.x, vec3(1.0, 0.0, 0.0));
			r *= rotate(mat4(1.0), model.r.y, vec3(0.0, 1.0, 0.0));
			r *= rotate(mat4(1.0), model.r.z, vec3(0.0, 0.0, 1.0));
			s = scale(mat4(1.0), model.s);
			t = translate(mat4(1.0), model.t);

			glUniformMatrix4fv(depth.modelUm4m, 1, GL_FALSE, value_ptr(t * r * s));
			glUniformMatrix4fv(depth.modelUm4v, 1, GL_FALSE, value_ptr(view));
			glUniformMatrix4fv(depth.modelUm4p, 1, GL_FALSE, value_ptr(proj));

			for (int i = 0; i < model.shapes.size(); i++) {
				int m = model.shapes[i].materialID;
				glBindVertexArray(model.shapes[i].vao);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, model.materials[m].diffuse_tex);
				glUniform1i(depth.modelTexLoc, 0);

				glEnableVertexAttribArray(0);
				glEnableVertexAttribArray(1);
				glDrawElements(GL_TRIANGLES, model.shapes[i].drawCount, GL_UNSIGNED_INT, 0);
				glBindVertexArray(0);
			}
		}
	}
	glUseProgram(0);
}

bool loadModel(const string& path) {

	model tmpModel;

	const aiScene* scene = aiImportFile(path.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_CalcTangentSpace);

	if (!scene) {
		cout << "load scene error!\n";
		return false;
	}

	for (unsigned int i = 0; i < scene->mNumMaterials; i++) {
		aiMaterial* material = scene->mMaterials[i];
		Material tmpMaterial;
		aiString texturePath;
		aiColor3D color;
		//ka, kd, ks
		if (material->Get(AI_MATKEY_COLOR_AMBIENT, color) == aiReturn_SUCCESS) {
			tmpMaterial.Ka = vec3((float)color[0], (float)color[1], (float)color[2]);
		}
		if (material->Get(AI_MATKEY_COLOR_DIFFUSE, color) == aiReturn_SUCCESS) {
			tmpMaterial.Kd = vec3((float)color[0], (float)color[1], (float)color[2]);
		}
		if (material->Get(AI_MATKEY_COLOR_SPECULAR, color) == aiReturn_SUCCESS) {
			tmpMaterial.Ks = vec3((float)color[0], (float)color[1], (float)color[2]);
		}

		//texture
		if (material->GetTexture(aiTextureType_AMBIENT, 0, &texturePath) == aiReturn_SUCCESS) {
			cout << texturePath.C_Str() << " am\n";
			texture_data tdata = loadImg(texturePath.C_Str());
			glGenTextures(1, &tmpMaterial.ambient_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.ambient_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == aiReturn_SUCCESS) {
			cout << texturePath.C_Str() << " df\n";
			texture_data tdata = loadImg(texturePath.C_Str());
			glGenTextures(1, &tmpMaterial.diffuse_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.diffuse_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			cout << "default diffuse map\n";
			texture_data tdata = loadImg("default_diff_map.png");
			if (tdata.data != NULL)cout << "yes\n";
			glGenTextures(1, &tmpMaterial.diffuse_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.diffuse_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		if (material->GetTexture(aiTextureType_SPECULAR, 0, &texturePath) == aiReturn_SUCCESS) {
			cout << texturePath.C_Str() << " sp\n";
			texture_data tdata = loadImg(texturePath.C_Str());
			glGenTextures(1, &tmpMaterial.spec_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.spec_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			cout << "default sp map\n";
			texture_data tdata = loadImg("default_sp_map.png");
			glGenTextures(1, &tmpMaterial.spec_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.spec_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}

		if (material->GetTexture(aiTextureType_HEIGHT, 0, &texturePath) == aiReturn_SUCCESS) {
			cout << texturePath.C_Str() << " bp\n";
			texture_data tdata = loadImg(texturePath.C_Str());
			glGenTextures(1, &tmpMaterial.normal_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.normal_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		else {
			cout << "default bump map\n";
			texture_data tdata = loadImg("default_normal_map.png");
			glGenTextures(1, &tmpMaterial.normal_tex);
			glBindTexture(GL_TEXTURE_2D, tmpMaterial.normal_tex);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
			glGenerateMipmap(GL_TEXTURE_2D);
		}
		tmpModel.materials.push_back(tmpMaterial);
	}

	for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[i];
		Shape tmpShape;
		vector<GLfloat> vp, vn, vt, tan;
		vector<unsigned int> ve;
		glGenVertexArrays(1, &tmpShape.vao);
		glBindVertexArray(tmpShape.vao);
		glGenBuffers(1, &tmpShape.vbo_position);
		glGenBuffers(1, &tmpShape.vbo_normal);
		glGenBuffers(1, &tmpShape.vbo_texcoord);
		glGenBuffers(1, &tmpShape.vbo_tangent);
		glGenBuffers(1, &tmpShape.ebo);
		for (unsigned int j = 0; j < mesh->mNumVertices; j++) {
			vp.push_back(mesh->mVertices[j][0]);
			vp.push_back(mesh->mVertices[j][1]);
			vp.push_back(mesh->mVertices[j][2]);

			vn.push_back(mesh->mNormals[j][0]);
			vn.push_back(mesh->mNormals[j][1]);
			vn.push_back(mesh->mNormals[j][2]);

			tan.push_back(mesh->mTangents[j][0]);
			tan.push_back(mesh->mTangents[j][1]);
			tan.push_back(mesh->mTangents[j][2]);

			tan.push_back(mesh->mBitangents[j][0]);
			tan.push_back(mesh->mBitangents[j][1]);
			tan.push_back(mesh->mBitangents[j][2]);

			if (mesh->mTextureCoords[0] != NULL) {
				vt.push_back(mesh->mTextureCoords[0][j][0]);
				vt.push_back(mesh->mTextureCoords[0][j][1]);
			}
			else {
				vt.push_back(0.0f);
				vt.push_back(0.0f);
			}
		}

		vp.shrink_to_fit();
		vt.shrink_to_fit();
		vn.shrink_to_fit();

		glBindBuffer(GL_ARRAY_BUFFER, tmpShape.vbo_position);
		glBufferData(GL_ARRAY_BUFFER, vp.size() * sizeof(GL_FLOAT), &vp.at(0), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, tmpShape.vbo_texcoord);
		glBufferData(GL_ARRAY_BUFFER, vt.size() * sizeof(GL_FLOAT), &vt.at(0), GL_STATIC_DRAW);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, tmpShape.vbo_normal);
		glBufferData(GL_ARRAY_BUFFER, vn.size() * sizeof(GL_FLOAT), &vn.at(0), GL_STATIC_DRAW);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, tmpShape.vbo_tangent);
		glBufferData(GL_ARRAY_BUFFER, tan.size() * sizeof(GL_FLOAT), &tan.at(0), GL_STATIC_DRAW);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), 0);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GL_FLOAT), (GLvoid*)(3 * sizeof(GL_FLOAT)));

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);
		glEnableVertexAttribArray(4);

		for (unsigned int j = 0; j < mesh->mNumFaces; j++) {
			ve.push_back(mesh->mFaces[j].mIndices[0]);
			ve.push_back(mesh->mFaces[j].mIndices[1]);
			ve.push_back(mesh->mFaces[j].mIndices[2]);
		}
		ve.shrink_to_fit();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tmpShape.ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ve.size() * sizeof(unsigned int), &ve.at(0), GL_STATIC_DRAW);

		tmpShape.materialID = mesh->mMaterialIndex;
		tmpShape.drawCount = mesh->mNumFaces * 3;

		tmpModel.shapes.push_back(tmpShape);
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

	}
	tmpModel.r = vec3(0.0, 0.0, 0.0);
	tmpModel.t = vec3(-10.0, -13.0, -8.0);
	tmpModel.s = vec3(0.2, 0.2, 0.2);
	models.push_back(tmpModel);

	aiReleaseImport(scene);
	return true;
}

char** loadShaderSource(const char* file)
{
	FILE* fp = fopen(file, "rb");
	fseek(fp, 0, SEEK_END);
	long sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char *src = new char[sz + 1];
	fread(src, sizeof(char), sz, fp);
	src[sz] = '\0';
	char **srcp = new char*[1];
	srcp[0] = src;
	return srcp;
}

void freeShaderSource(char** srcp)
{
	delete[] srcp[0];
	delete[] srcp;
}

void createDepthRenderer() {
	char** vs = NULL;
	char** fs = NULL;
	char** tcs = NULL;
	char** tes = NULL;
	char** gs = NULL;

	depth.programModel = glCreateProgram();

	vs = loadShaderSource("modelDep.vs.glsl");
	fs = loadShaderSource("modelDep.fs.glsl");

	GLuint depthVS = glCreateShader(GL_VERTEX_SHADER);
	GLuint depthFS = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(depthVS, 1, vs, NULL);
	glShaderSource(depthFS, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(depthVS);
	glCompileShader(depthFS);

	shaderLog(depthVS);
	shaderLog(depthFS);

	glAttachShader(depth.programModel, depthVS);
	glAttachShader(depth.programModel, depthFS);
	glLinkProgram(depth.programModel);

	depth.modelUm4m = glGetUniformLocation(depth.programModel, "um4m");
	depth.modelUm4v = glGetUniformLocation(depth.programModel, "um4v");
	depth.modelUm4p = glGetUniformLocation(depth.programModel, "um4p");
	depth.modelClipLoc = glGetUniformLocation(depth.programModel, "plane");
	depth.modelTexLoc = glGetUniformLocation(depth.programModel, "tex");

	//terrain depth
	depth.programTerrain = glCreateProgram();

	vs = loadShaderSource("terrainDep.vs.glsl");
	fs = loadShaderSource("terrainDep.fs.glsl");
	tcs = loadShaderSource("terrainDep.tcs.glsl");
	tes = loadShaderSource("terrainDep.tes.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint TCSShader = glCreateShader(GL_TESS_CONTROL_SHADER);
	GLuint TESShader = glCreateShader(GL_TESS_EVALUATION_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);
	glShaderSource(TCSShader, 1, tcs, NULL);
	glShaderSource(TESShader, 1, tes, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);
	freeShaderSource(tcs);
	freeShaderSource(tes);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);
	glCompileShader(TCSShader);
	glCompileShader(TESShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);
	shaderLog(TCSShader);
	shaderLog(TESShader);

	glAttachShader(depth.programTerrain, vertexShader);
	glAttachShader(depth.programTerrain, fragmentShader);
	glAttachShader(depth.programTerrain, TCSShader);
	glAttachShader(depth.programTerrain, TESShader);
	glLinkProgram(depth.programTerrain);

	glUseProgram(depth.programTerrain);

	depth.terrainUm4m = glGetUniformLocation(depth.programTerrain, "um4m");
	depth.terrainUm4v = glGetUniformLocation(depth.programTerrain, "um4v");
	depth.terrainUm4p = glGetUniformLocation(depth.programTerrain, "um4p");
	depth.terrainClipLoc = glGetUniformLocation(depth.programTerrain, "plane");
	depth.terrainHeightMapLoc = glGetUniformLocation(depth.programTerrain, "heightMap");
	depth.terrainTCSMvpLoc = glGetUniformLocation(depth.programTerrain, "mvp_matrix");

	glDeleteShader(depthVS);
	glDeleteShader(depthFS);

	glGenFramebuffers(1, &depthFBO);
}

void createCubeRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	//cube program
	programCube = glCreateProgram();

	vs = loadShaderSource("cubeVS.vs.glsl");
	fs = loadShaderSource("cubeFS.fs.glsl");

	GLuint cubeVS = glCreateShader(GL_VERTEX_SHADER);
	GLuint cubeFS = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(cubeVS, 1, vs, NULL);
	glShaderSource(cubeFS, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(cubeVS);
	glCompileShader(cubeFS);

	shaderLog(cubeVS);
	shaderLog(cubeFS);

	glAttachShader(programCube, cubeVS);
	glAttachShader(programCube, cubeFS);
	glLinkProgram(programCube);

	cubeTexLoc = glGetUniformLocation(programCube, "cubemap");
	cubemv = glGetUniformLocation(programCube, "um4mv");
	cubep = glGetUniformLocation(programCube, "um4p");
	cubeLightStrLoc = glGetUniformLocation(programCube, "lightStr");

	glUseProgram(programCube);
	glDeleteShader(cubeVS);
	glDeleteShader(cubeFS);


	glGenVertexArrays(1, &cubeVAO);
	glBindVertexArray(cubeVAO);

	glGenBuffers(1, &cubeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);

	glGenTextures(1, &cubeTex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
	texture_data tData;
	for (int i = 0; i < 6; i++) {
		tData = loadImg(cubeMaps[i]);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, tData.width, tData.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tData.data);
		free(tData.data);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void createWindowRenderer() {
	char** fboVs = NULL;
	char** fboFs = NULL;

	window.program = glCreateProgram();

	fboVs = loadShaderSource("FBOvs.vs.glsl");
	fboFs = loadShaderSource("FBOfs.fs.glsl");

	GLuint vertexShaderFBO = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderFBO = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShaderFBO, 1, fboVs, NULL);
	glShaderSource(fragmentShaderFBO, 1, fboFs, NULL);

	freeShaderSource(fboVs);
	freeShaderSource(fboFs);

	glCompileShader(vertexShaderFBO);
	glCompileShader(fragmentShaderFBO);

	shaderLog(vertexShaderFBO);
	shaderLog(fragmentShaderFBO);

	glAttachShader(window.program, vertexShaderFBO);
	glAttachShader(window.program, fragmentShaderFBO);
	glLinkProgram(window.program);

	glUseProgram(window.program);
	glDeleteShader(vertexShaderFBO);
	glDeleteShader(fragmentShaderFBO);

	window.fboTexLoc = glGetUniformLocation(window.program, "tex");

	glGenFramebuffers(1, &window.FBO);

	glGenVertexArrays(1, &window.VAO);

	glBindVertexArray(window.VAO);
	glGenBuffers(1, &window.VBO);
	glBindBuffer(GL_ARRAY_BUFFER, window.VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(windowPos), windowPos, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GL_FLOAT), 0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GL_FLOAT), (GLvoid*)(3 * sizeof(GL_FLOAT)));
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

}

void createModelRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	modelRenderer.program = glCreateProgram();

	vs = loadShaderSource("vertex.vs.glsl");
	fs = loadShaderSource("fragment.fs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(modelRenderer.program, vertexShader);
	glAttachShader(modelRenderer.program, fragmentShader);
	glLinkProgram(modelRenderer.program);

	glUseProgram(modelRenderer.program);

	modelRenderer.um4m = glGetUniformLocation(modelRenderer.program, "um4m");
	modelRenderer.um4v = glGetUniformLocation(modelRenderer.program, "um4v");
	modelRenderer.um4p = glGetUniformLocation(modelRenderer.program, "um4p");
	modelRenderer.amTexLoc = glGetUniformLocation(modelRenderer.program, "amTex");
	modelRenderer.dfTexLoc = glGetUniformLocation(modelRenderer.program, "dfTex");
	modelRenderer.spTexLoc = glGetUniformLocation(modelRenderer.program, "spTex");
	modelRenderer.normalTexLoc = glGetUniformLocation(modelRenderer.program, "normalTex");
	modelRenderer.envMapLoc = glGetUniformLocation(modelRenderer.program, "envMap");
	modelRenderer.shadowMatLoc = glGetUniformLocation(modelRenderer.program, "shadowMat");
	modelRenderer.shadowTexLoc = glGetUniformLocation(modelRenderer.program, "shadowTex");
	modelRenderer.kaLoc = glGetUniformLocation(modelRenderer.program, "lightParam.Ka");
	modelRenderer.kdLoc = glGetUniformLocation(modelRenderer.program, "lightParam.Kd");
	modelRenderer.ksLoc = glGetUniformLocation(modelRenderer.program, "lightParam.Ks");
	modelRenderer.camPosLoc = glGetUniformLocation(modelRenderer.program, "camPos");
	modelRenderer.clipPlaneLoc = glGetUniformLocation(modelRenderer.program, "plane");
	modelRenderer.shadowMatLoc = glGetUniformLocation(modelRenderer.program, "shadowMat");
	modelRenderer.shadowTexLoc = glGetUniformLocation(modelRenderer.program, "shadowTex");
	modelRenderer.lightStrLoc = glGetUniformLocation(modelRenderer.program, "lightStr");

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glUseProgram(0);
	glBindVertexArray(0);
}

void createQuadRenderer() {
	char** vs = NULL;
	char** fs = NULL;
	char** tcs = NULL;
	char** tes = NULL;
	char** gs = NULL;

	quad.program = glCreateProgram();

	vs = loadShaderSource("quad.vs.glsl");
	fs = loadShaderSource("quad.fs.glsl");
	tcs = loadShaderSource("quad.tcs.glsl");
	tes = loadShaderSource("quad.tes.glsl");
	gs = loadShaderSource("quad.gs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint TCSShader = glCreateShader(GL_TESS_CONTROL_SHADER);
	GLuint TESShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
	GLuint GeoShader = glCreateShader(GL_GEOMETRY_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);
	glShaderSource(TCSShader, 1, tcs, NULL);
	glShaderSource(TESShader, 1, tes, NULL);
	glShaderSource(GeoShader, 1, gs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);
	freeShaderSource(tcs);
	freeShaderSource(tes);
	freeShaderSource(gs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);
	glCompileShader(TCSShader);
	glCompileShader(TESShader);
	glCompileShader(GeoShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);
	shaderLog(TCSShader);
	shaderLog(TESShader);
	shaderLog(GeoShader);

	glAttachShader(quad.program, vertexShader);
	glAttachShader(quad.program, fragmentShader);
	glAttachShader(quad.program, TCSShader);
	glAttachShader(quad.program, TESShader);
	//glAttachShader(quad.program, GeoShader);
	glLinkProgram(quad.program);

	glUseProgram(quad.program);

	quad.um4m = glGetUniformLocation(quad.program, "um4m");
	quad.um4v = glGetUniformLocation(quad.program, "um4v");
	quad.um4p = glGetUniformLocation(quad.program, "um4p");
	quad.tcsMvpLoc = glGetUniformLocation(quad.program, "mvp_matrix");
	quad.clipPlaneLoc = glGetUniformLocation(quad.program, "plane");
	terrain.texLoc = glGetUniformLocation(quad.program, "heightMap");
	terrain.normalLoc = glGetUniformLocation(quad.program, "normalMap");
	terrain.landTexLoc = glGetUniformLocation(quad.program, "landTex");
	quad.shadowMatLoc = glGetUniformLocation(quad.program, "shadowMat");
	quad.shadowTexLoc = glGetUniformLocation(quad.program, "shadowTex");
	quad.camPosLoc = glGetUniformLocation(quad.program, "camPos");
	quad.moveFactorLoc = glGetUniformLocation(quad.program, "moveFactor");
	quad.dudvMapLoc = glGetUniformLocation(quad.program, "dudvMap");
	quad.waterNormalLoc = glGetUniformLocation(quad.program, "waterNormal");
	quad.lightStrLoc = glGetUniformLocation(quad.program, "lightStr");

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glGenVertexArrays(1, &quadVAO);
	glBindVertexArray(quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glEnable(GL_TEXTURE_2D);
	texture_data t = loadImg("hm2.png");
	glGenTextures(1, &terrain.heightMap);
	glBindTexture(GL_TEXTURE_2D, terrain.heightMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	t = loadImg("NormalMap.png");
	glGenTextures(1, &terrain.normalMap);
	glBindTexture(GL_TEXTURE_2D, terrain.normalMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	t = loadImg("landtex.jpg");
	glGenTextures(1, &terrain.landTex);
	glBindTexture(GL_TEXTURE_2D, terrain.landTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glUseProgram(0);
	glBindVertexArray(0);
}

void createWaterRenderer() {
	char** vs = NULL;
	char** fs = NULL;
	char** tcs = NULL;
	char** tes = NULL;
	char** gs = NULL;

	water.program = glCreateProgram();

	vs = loadShaderSource("water.vs.glsl");
	fs = loadShaderSource("water.fs.glsl");
	tcs = loadShaderSource("water.tcs.glsl");
	tes = loadShaderSource("water.tes.glsl");
	gs = loadShaderSource("water.gs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	GLuint TCSShader = glCreateShader(GL_TESS_CONTROL_SHADER);
	GLuint TESShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
	GLuint GeoShader = glCreateShader(GL_GEOMETRY_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);
	glShaderSource(TCSShader, 1, tcs, NULL);
	glShaderSource(TESShader, 1, tes, NULL);
	glShaderSource(GeoShader, 1, gs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);
	freeShaderSource(tcs);
	freeShaderSource(tes);
	freeShaderSource(gs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);
	glCompileShader(TCSShader);
	glCompileShader(TESShader);
	glCompileShader(GeoShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);
	shaderLog(TCSShader);
	shaderLog(TESShader);
	shaderLog(GeoShader);


	glAttachShader(water.program, vertexShader);
	glAttachShader(water.program, fragmentShader);
	glAttachShader(water.program, TCSShader);
	glAttachShader(water.program, TESShader);
	//glAttachShader(water.program, GeoShader);
	glLinkProgram(water.program);

	glUseProgram(water.program);

	water.um4v = glGetUniformLocation(water.program, "um4v");
	water.um4p = glGetUniformLocation(water.program, "um4p");
	water.texRFRLoc = glGetUniformLocation(water.program, "texRFR");
	water.texRFLLoc = glGetUniformLocation(water.program, "texRFL");
	water.levelLoc = glGetUniformLocation(water.program, "level");
	water.dudvLoc = glGetUniformLocation(water.program, "dudvMap");
	water.moveFactorLoc = glGetUniformLocation(water.program, "moveFactor");
	water.normalMapLoc = glGetUniformLocation(water.program, "normalMap");
	water.camPosLoc = glGetUniformLocation(water.program, "camPos");
	water.tcsMvpLoc = glGetUniformLocation(water.program, "mvp_matrix");
	water.disMapLoc = glGetUniformLocation(water.program, "disMap");
	water.shadowMatLoc = glGetUniformLocation(water.program, "shadowMat");
	water.shadowTexLoc = glGetUniformLocation(water.program, "shadowTex");
	water.lightStrLoc = glGetUniformLocation(water.program, "lightStr");

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteShader(TCSShader);
	glDeleteShader(TESShader);
	glDeleteShader(GeoShader);

	glEnable(GL_TEXTURE_2D);
	texture_data t = loadImg("waterDUDV.png");
	glGenTextures(1, &water.texDUDV);
	glBindTexture(GL_TEXTURE_2D, water.texDUDV);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	t = loadImg("waterNormalMap.png");
	glGenTextures(1, &water.normalMap);
	glBindTexture(GL_TEXTURE_2D, water.normalMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	t = loadImg("waterDisplaceMap.png");
	glGenTextures(1, &water.disMap);
	glBindTexture(GL_TEXTURE_2D, water.disMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glUseProgram(0);
	glBindVertexArray(0);
}

void createGrassRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	grass.program = glCreateProgram();

	vs = loadShaderSource("grass.vs.glsl");
	fs = loadShaderSource("grass.fs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(grass.program, vertexShader);
	glAttachShader(grass.program, fragmentShader);
	glLinkProgram(grass.program);

	glUseProgram(grass.program);

	grass.heightMapLoc = glGetUniformLocation(grass.program, "heightMap");
	grass.moveFactorLoc = glGetUniformLocation(grass.program, "moveFactor");
	grass.noiseTexLoc = glGetUniformLocation(grass.program, "noiseMap");
	grass.mvpLoc = glGetUniformLocation(grass.program, "mvp");
	grass.dfTexLoc = glGetUniformLocation(grass.program, "dfTex");
	grass.shadowTexLoc = glGetUniformLocation(grass.program, "shadowTex");
	grass.shadowMatLoc = glGetUniformLocation(grass.program, "shadowMat");
	grass.lightStrLoc = glGetUniformLocation(grass.program, "lightStr");

	texture_data t = loadImg("grass.png");
	glGenTextures(1, &grass.dfTex);
	glBindTexture(GL_TEXTURE_2D, grass.dfTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	t = loadImg("noise.png");
	glGenTextures(1, &grass.noiseTex);
	glBindTexture(GL_TEXTURE_2D, grass.noiseTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, t.width, t.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, t.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

}

void createDepthFieldRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	depField.program = glCreateProgram();

	vs = loadShaderSource("FBOvs.vs.glsl");
	fs = loadShaderSource("depField.fs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(depField.program, vertexShader);
	glAttachShader(depField.program, fragmentShader);
	glLinkProgram(depField.program);

	glUseProgram(depField.program);

	depField.blurTexLoc = glGetUniformLocation(depField.program, "blurTex");
	depField.texLoc = glGetUniformLocation(depField.program, "tex");
	depField.viewDepTexLoc = glGetUniformLocation(depField.program, "viewDepTex");
	depField.ssaoTexLoc = glGetUniformLocation(depField.program, "ssaoTex");


	//blur program
	depField.programBlur = glCreateProgram();

	vs = loadShaderSource("FBOvs.vs.glsl");
	fs = loadShaderSource("blur.fs.glsl");

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(depField.programBlur, vertexShader);
	glAttachShader(depField.programBlur, fragmentShader);
	glLinkProgram(depField.programBlur);

	glUseProgram(depField.program);

	depField.imgSizeLoc = glGetUniformLocation(depField.programBlur, "img_size");
	depField.texLoc_for_blur = glGetUniformLocation(depField.programBlur, "tex");

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glUseProgram(0);
}

void createSSAORender() {
	char** vs_source = NULL;
	char** fs_source = NULL;
	vs_source = loadShaderSource("ssao.vs.glsl");
	fs_source = loadShaderSource("ssao.fs.glsl");

	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, vs_source, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, fs_source, NULL);
	glCompileShader(fs);

	shaderLog(vs);
	shaderLog(fs);

	ssao.program = glCreateProgram();
	glAttachShader(ssao.program, vs);
	glAttachShader(ssao.program, fs);
	glLinkProgram(ssao.program);

	glUseProgram(ssao.program);

	ssao.depth_mapLoc = glGetUniformLocation(ssao.program, "depth_map");
	ssao.normal_mapLoc = glGetUniformLocation(ssao.program, "normal_map");
	ssao.noise_mapLoc = glGetUniformLocation(ssao.program, "noise_map");
	ssao.projLoc = glGetUniformLocation(ssao.program, "proj");
	ssao.noise_scaleLoc - glGetUniformLocation(ssao.program, "noise_scale");
	ssao.blockIdx = glGetUniformBlockIndex(ssao.program, "Kernals");
	glUniformBlockBinding(ssao.program, ssao.blockIdx, 0);
	glGenVertexArrays(1, &ssao.vao);
	glBindVertexArray(ssao.vao);

	// init ubo
	glGenBuffers(1, &ssao.kernal_ubo);
	glBindBuffer(GL_UNIFORM_BUFFER, ssao.kernal_ubo);
	vec4 kernals[32];
	for (int i = 0; i < 32; ++i)
	{
		float scale = i / 32.0;
		scale = 0.1f + 0.9f * scale * scale;
		kernals[i] = vec4(normalize(vec3(
			rand() / (float)RAND_MAX * 2.0f - 1.0f,
			rand() / (float)RAND_MAX * 2.0f - 1.0f,
			rand() / (float)RAND_MAX * 0.85f + 0.15f)) * scale,
			0.0f
		);
	}
	glBufferData(GL_UNIFORM_BUFFER, 32 * sizeof(vec4), &kernals[0][0], GL_STATIC_DRAW);

	// init noise_map
	glGenTextures(1, &ssao.noise_map);
	glBindTexture(GL_TEXTURE_2D, ssao.noise_map);
	vec3 noiseData[16];
	for (int i = 0; i < 16; ++i)
	{
		noiseData[i] = normalize(vec3(
			rand() / (float)RAND_MAX, // 0.0 ~ 1.0
			rand() / (float)RAND_MAX, // 0.0 ~ 1.0
			0.0f
		));
	}
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 4, 4, 0, GL_RGB, GL_FLOAT, &noiseData[0][0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void createCameraLineRenderer() {
	char** vs = NULL;
	char** fs = NULL;
	char** gs = NULL;

	//cube program
	cameraLine.program = glCreateProgram();

	vs = loadShaderSource("cameraLine.vs.glsl");
	fs = loadShaderSource("cameraLine.fs.glsl");
	//gs = loadShaderSource("cameraLine.gs.glsl");

	GLuint cubeVS = glCreateShader(GL_VERTEX_SHADER);
	GLuint cubeFS = glCreateShader(GL_FRAGMENT_SHADER);
	//GLuint GS = glCreateShader(GL_GEOMETRY_SHADER);

	glShaderSource(cubeVS, 1, vs, NULL);
	glShaderSource(cubeFS, 1, fs, NULL);
	//glShaderSource(GS, 1, gs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);
	//freeShaderSource(gs);

	glCompileShader(cubeVS);
	glCompileShader(cubeFS);
	//glCompileShader(GS);

	shaderLog(cubeVS);
	shaderLog(cubeFS);
	//shaderLog(GS);

	glAttachShader(cameraLine.program, cubeVS);
	glAttachShader(cameraLine.program, cubeFS);
	//glAttachShader(cameraLine.program, GS);
	glLinkProgram(cameraLine.program);

	cameraLine.mvpLoc = glGetUniformLocation(cameraLine.program, "mvp");

	glUseProgram(cameraLine.program);
	glDeleteShader(cubeVS);
	glDeleteShader(cubeFS);
	//glDeleteShader(GS);


	glGenVertexArrays(1, &cameraLine.VAO);
	glBindVertexArray(cameraLine.VAO);

	glGenBuffers(1, &cameraLine.VBO);
	glBindBuffer(GL_ARRAY_BUFFER, cameraLine.VBO);
	glBufferData(GL_ARRAY_BUFFER, 100 * sizeof(float), NULL, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	cameraLine.numPts = 0;
}

void reshapeDepthFBO() {
	glDeleteTextures(1, &shadowDepTex);
	glBindFramebuffer(GL_FRAMEBUFFER, depthFBO);

	glGenTextures(1, &shadowDepTex);
	glBindTexture(GL_TEXTURE_2D, shadowDepTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, 8192, 8192, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowDepTex, 0);
}

void reshapeWindowFBO(int width, int height) {
	glDeleteTextures(1, &window.texFBO);
	glDeleteTextures(1, &depField.texBlur);
	glDeleteTextures(1, &depField.viewDepTex);
	glDeleteTextures(1, &gbuffer.normal_map);
	glDeleteTextures(1, &ssao.tex);

	glBindFramebuffer(GL_FRAMEBUFFER, window.FBO);

	glGenTextures(1, &window.texFBO);
	glBindTexture(GL_TEXTURE_2D, window.texFBO);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &depField.texBlur);
	glBindTexture(GL_TEXTURE_2D, depField.texBlur);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &depField.viewDepTex);
	glBindTexture(GL_TEXTURE_2D, depField.viewDepTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenTextures(1, &gbuffer.normal_map);
	glBindTexture(GL_TEXTURE_2D, gbuffer.normal_map);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &ssao.tex);
	glBindTexture(GL_TEXTURE_2D, ssao.tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, window.texFBO, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gbuffer.normal_map, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, depField.texBlur, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, ssao.tex, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depField.viewDepTex, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void createRainRender() {

	char** render_vs_source = NULL;
	char** render_fs_source = NULL;
	render_vs_source = loadShaderSource("rainvs.vs.glsl");
	render_fs_source = loadShaderSource("rainfs.fs.glsl");

	// Create shader program for rendering particles.
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, render_vs_source, NULL);
	glCompileShader(vs);
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, render_fs_source, NULL);
	glCompileShader(fs);

	shaderLog(vs);
	shaderLog(fs);

	rain.renderProgram = glCreateProgram();
	glAttachShader(rain.renderProgram, vs);
	glAttachShader(rain.renderProgram, fs);
	glLinkProgram(rain.renderProgram);

	glUseProgram(rain.renderProgram);
	rain.mvLoc = glGetUniformLocation(rain.renderProgram, "mv");
	rain.pLoc = glGetUniformLocation(rain.renderProgram, "p");
	rain.velocityLoc = glGetUniformLocation(rain.renderProgram, "velocity");
	rain.noiseLoc = glGetUniformLocation(rain.renderProgram, "noise");

	glGenVertexArrays(1, &rain.vao);
	glBindVertexArray(rain.vao);

	glGenBuffers(1, &rain.buffer);
	glBindBuffer(GL_ARRAY_BUFFER, rain.buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(raindrop), raindrop, GL_STATIC_DRAW);


	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	rain.velocity = vec3(0.0, -0.5, 0.0);
}

void createWaterColorRenderer() {
	char** fboVs = NULL;
	char** fboFs = NULL;

	programWaterColor = glCreateProgram();

	fboVs = loadShaderSource("FBOvs.vs.glsl");
	fboFs = loadShaderSource("waterColorfs.fs.glsl");

	GLuint vertexShaderFBO = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderFBO = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShaderFBO, 1, fboVs, NULL);
	glShaderSource(fragmentShaderFBO, 1, fboFs, NULL);

	freeShaderSource(fboVs);
	freeShaderSource(fboFs);

	glCompileShader(vertexShaderFBO);
	glCompileShader(fragmentShaderFBO);

	shaderLog(vertexShaderFBO);
	shaderLog(fragmentShaderFBO);

	glAttachShader(programWaterColor, vertexShaderFBO);
	glAttachShader(programWaterColor, fragmentShaderFBO);
	glLinkProgram(programWaterColor);

	glDeleteShader(vertexShaderFBO);
	glDeleteShader(fragmentShaderFBO);


	//load noise map
	texture_data tdata = loadImg("noise_t.png");

	glGenTextures(1, &noise_map);
	glBindTexture(GL_TEXTURE_2D, noise_map);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glGenerateMipmap(GL_TEXTURE_2D);
}

void renderWaterColorTexture() {
	glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, windowWidth, windowHeight);

	glBindVertexArray(window.VAO);


	glUseProgram(programWaterColor);

	glUniform1i(glGetUniformLocation(programWaterColor, "ssaoEn"), ssaoFlag ? 1 : 0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, window.texFBO);
	glUniform1i(glGetUniformLocation(programWaterColor, "tex"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, noise_map);
	glUniform1i(glGetUniformLocation(programWaterColor, "noise_tex"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, ssao.tex);
	glUniform1i(glGetUniformLocation(programWaterColor, "ssaoTex"), 2);

	float img_sz[2] = { (float)windowWidth, (float)windowHeight };
	glUniform2fv(glGetUniformLocation(programWaterColor, "img_sz"), 1, img_sz);
	glUniform1i(glGetUniformLocation(programWaterColor, "en"), effect);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glUseProgram(0);
	glBindVertexArray(0);
}

mat4 sword_model_matrix;
void loadSword()
{
	tinyobj::attrib_t attrib;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string warn;
	string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, "sword.obj");
	if (!warn.empty()) {
		cout << warn << endl;
	}
	if (!err.empty()) {
		cout << err << endl;
	}
	if (!ret) {
		exit(1);
	}

	Sword.drawCount = 0;
	vector<float> vertices, texcoords, normals;  // if OBJ preserves vertex order, you can use element array buffer for memory efficiency
	for (int s = 0; s < shapes.size(); ++s) {  // for 'ladybug.obj', there is only one object
		int index_offset = 0;
		for (int f = 0; f < shapes[s].mesh.num_face_vertices.size(); ++f) {
			int fv = shapes[s].mesh.num_face_vertices[f];
			for (int v = 0; v < fv; ++v) {
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 0]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 1]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 2]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 0]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 0]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 2]);
			}
			index_offset += fv;
			Sword.drawCount += fv;
		}
	}

	glGenVertexArrays(1, &Sword.vao);
	glBindVertexArray(Sword.vao);

	glGenBuffers(1, &Sword.vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, Sword.vbo_position);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float) + normals.size() * sizeof(float), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(float), vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), texcoords.size() * sizeof(float), texcoords.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float), normals.size() * sizeof(float), normals.data());

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float) + texcoords.size() * sizeof(float)));
	glEnableVertexAttribArray(2);

	shapes.clear();
	shapes.shrink_to_fit();
	materials.clear();
	materials.shrink_to_fit();
	vertices.clear();
	vertices.shrink_to_fit();
	texcoords.clear();
	texcoords.shrink_to_fit();
	normals.clear();
	normals.shrink_to_fit();

	//cout << "Load " << Sword.drawCount << " vertices" << endl;
	texture_data tdata = loadImg("Sting_Base_Color.png");

	glGenTextures(1, &Sting_Base_Color);
	glBindTexture(GL_TEXTURE_2D, Sting_Base_Color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, tdata.width, tdata.height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, (void*)tdata.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
}

void createSwordRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	programSword = glCreateProgram();

	vs = loadShaderSource("Sword.vs.glsl");
	fs = loadShaderSource("Sword.fs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(programSword, vertexShader);
	glAttachShader(programSword, fragmentShader);
	glLinkProgram(programSword);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderSword() {
	/*mat4 model = translate(mat4(1.0f), vec3(27.0, 2.0, 10.5)) *
		rotate(mat4(1.0f), radians(30.0f), vec3(0.0, 0.0, 1.0)) *
		rotate(mat4(1.0f), radians(45.0f), vec3(1.0, 0.0, 0.0)) * scale(mat4(1.0f), vec3(2.0));*/
	sword_model_matrix = translate(mat4(1.0f), vec3(27.0, 4.0, 10.5)) *
		rotate(mat4(1.0f), radians(-20.0f), vec3(0.0, 0.0, 1.0)) *
		rotate(mat4(1.0f), radians(-90.0f), vec3(1.0, 0.0, 0.0)) * scale(mat4(1.0f), vec3(0.1));
	float cam_pos[3] = { camera.position.x, camera.position.y, camera.position.z };

	glUseProgram(programSword); 
	glUniformMatrix4fv(glGetUniformLocation(programSword, "shadow_matrix"), 1, GL_FALSE,
		value_ptr(shadowSbpv));
	glUniformMatrix4fv(glGetUniformLocation(programSword, "um4m"), 1, GL_FALSE,
		value_ptr(sword_model_matrix));
	glUniformMatrix4fv(glGetUniformLocation(programSword, "trans_inv_m"), 1, GL_FALSE,
		value_ptr(transpose(inverse(sword_model_matrix))));
	glUniformMatrix4fv(glGetUniformLocation(programSword, "um4v"), 1, GL_FALSE,
		value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(programSword, "um4p"), 1, GL_FALSE,
		value_ptr(proj));
	glUniform3fv(glGetUniformLocation(programSword, "camera_pos"), 1, cam_pos);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
	glUniform1i(glGetUniformLocation(programSword, "texCube"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, Sting_Base_Color);
	glUniform1i(glGetUniformLocation(programSword, "Sting_Base_Color"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, shadowDepTex);
	glUniform1i(glGetUniformLocation(programSword, "shadowTex"), 2);

	glBindVertexArray(Sword.vao);
	glDrawArrays(GL_TRIANGLES, 0, Sword.drawCount);

	glBindVertexArray(0);
	glUseProgram(0);
}

void loadSphere()
{
	tinyobj::attrib_t attrib;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string warn;
	string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, "Sphere.obj");
	if (!warn.empty()) {
		cout << warn << endl;
	}
	if (!err.empty()) {
		cout << err << endl;
	}
	if (!ret) {
		exit(1);
	}

	Sphere.drawCount = 0;
	vector<float> vertices, texcoords, normals;  // if OBJ preserves vertex order, you can use element array buffer for memory efficiency
	for (int s = 0; s < shapes.size(); ++s) {  // for 'ladybug.obj', there is only one object
		int index_offset = 0;
		for (int f = 0; f < shapes[s].mesh.num_face_vertices.size(); ++f) {
			int fv = shapes[s].mesh.num_face_vertices[f];
			for (int v = 0; v < fv; ++v) {
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 0]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 1]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 2]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 0]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 0]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 2]);
			}
			index_offset += fv;
			Sphere.drawCount += fv;
		}
	}

	glGenVertexArrays(1, &Sphere.vao);
	glBindVertexArray(Sphere.vao);

	glGenBuffers(1, &Sphere.vbo_position);
	glBindBuffer(GL_ARRAY_BUFFER, Sphere.vbo_position);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float) + normals.size() * sizeof(float), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(float), vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), texcoords.size() * sizeof(float), texcoords.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float), normals.size() * sizeof(float), normals.data());

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float) + texcoords.size() * sizeof(float)));
	glEnableVertexAttribArray(2);

	shapes.clear();
	shapes.shrink_to_fit();
	materials.clear();
	materials.shrink_to_fit();
	vertices.clear();
	vertices.shrink_to_fit();
	texcoords.clear();
	texcoords.shrink_to_fit();
	normals.clear();
	normals.shrink_to_fit();

	cout << "Load " << Sphere.drawCount << " vertices" << endl;
}

void createSphereRenderer() {
	char** vs = NULL;
	char** fs = NULL;

	programSphere = glCreateProgram();

	vs = loadShaderSource("sphere.vs.glsl");
	fs = loadShaderSource("sphere.fs.glsl");

	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, vs, NULL);
	glShaderSource(fragmentShader, 1, fs, NULL);

	freeShaderSource(vs);
	freeShaderSource(fs);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	glAttachShader(programSphere, vertexShader);
	glAttachShader(programSphere, fragmentShader);
	glLinkProgram(programSphere);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	glUseProgram(0);
	glBindVertexArray(0);
}

void renderSphere() {
	mat4 model = sword_model_matrix * translate(mat4(1.0f), vec3(0.0, 0.0, 28.0))
		* scale(mat4(1.0f), vec3(5.0, 2.5, 5.0));
	float cam_pos[3] = { camera.position.x, camera.position.y, camera.position.z };

	glUseProgram(programSphere);
	glUniformMatrix4fv(glGetUniformLocation(programSphere, "um4m"), 1, GL_FALSE,
		value_ptr(model));
	glUniformMatrix4fv(glGetUniformLocation(programSphere, "trans_inv_m"), 1, GL_FALSE,
		value_ptr(transpose(inverse(model))));
	glUniformMatrix4fv(glGetUniformLocation(programSphere, "um4v"), 1, GL_FALSE,
		value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(programSphere, "um4p"), 1, GL_FALSE,
		value_ptr(proj));
	glUniform3fv(glGetUniformLocation(programSphere, "camera_pos"), 1, cam_pos);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
	glUniform1i(glGetUniformLocation(programSphere, "texCube"), 0);

	glBindVertexArray(Sphere.vao);
	glDrawArrays(GL_TRIANGLES, 0, Sphere.drawCount);

	glBindVertexArray(0);
	glUseProgram(0);
}

void SSAOpass() {	
	glUseProgram(ssao.program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gbuffer.normal_map);
	glUniform1i(ssao.normal_mapLoc, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depField.viewDepTex);
	glUniform1i(ssao.depth_mapLoc, 1);
	glUniformMatrix4fv(ssao.projLoc, 1, GL_FALSE, value_ptr(proj));
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, ssao.noise_map);
	glUniform1i(ssao.noise_mapLoc, 2);
	glUniform2f(ssao.noise_scaleLoc, windowWidth / 4.0f, windowHeight / 4.0f);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, ssao.kernal_ubo);

	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(ssao.vao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void My_Init()
{

	vec3 lightPos = vec3(-40, 55.0, 25.0);
	lightProj = ortho(-50.0f, 50.0f, -50.0f, 50.0f, 0.0f, 100.0f);
	lightView = lookAt(lightPos, vec3(0.0f, 0.0f, 0.0f), vec3(0.0, 1.0, 0.0));

	camera.position = pts[0];
	camera.viewVec = viewCenterPts[0] - camera.position;
	camera.upVec = vec3(0.0, 1.0, 0.0);
	camera.moveFactor = 0;
	camera.moveSpeed = 0.0;

	rainflag = 0;
	//glClearColor(0.6f, 0.9f, 1.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	createModelRenderer();
	createWindowRenderer();
	createCubeRenderer();
	createDepthRenderer();
	createWaterRenderer();
	createQuadRenderer();
	createGrassRenderer();
	createRainRender();
	createDepthFieldRenderer();
	createSSAORender();
	createCameraLineRenderer();

	//add
	createWaterColorRenderer();
	
	loadSword();
	createSwordRenderer();

	loadSphere();
	createSphereRenderer();
	

	string treePath = "white_oak.obj";
	loadModel(treePath);

	glBindVertexArray(0);

	glGenFramebuffers(1, &water.FBO);
	water.level = water.moveFactor = 0.0;
}

void renderShadowMap() {
	glEnable(GL_DEPTH_TEST);

	glBindFramebuffer(GL_FRAMEBUFFER, depthFBO);
	glViewport(0, 0, 8192, 8192);
	glDrawBuffer(GL_NONE);

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(4.0f, 4.0f);

	mat4 camView = view;
	mat4 camProj = proj;

	view = lightView;
	proj = lightProj;

	glEnable(GL_CLIP_DISTANCE0);
	renderTerrainDep(vec4(0.0, 1.0, 0.0, -water.level));
	glDisable(GL_CLIP_DISTANCE0);
	renderModelsDep(vec4(0.0));
	renderGrass();
	renderSword();
	renderSphere();

	view = camView;
	proj = camProj;

	glDisable(GL_POLYGON_OFFSET_FILL);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, windowWidth, windowHeight);
}

void renderCubeBox() {
	view = lookAt(vec3(0.0), camera.viewVec, camera.upVec);
	glDepthMask(GL_FALSE);
	glUseProgram(programCube);

	glBindVertexArray(cubeVAO);
	glEnableVertexAttribArray(0);

	glUniformMatrix4fv(cubemv, 1, GL_FALSE, value_ptr(view));
	glUniformMatrix4fv(cubep, 1, GL_FALSE, value_ptr(proj));
	glUniform1f(cubeLightStrLoc, rainflag ? rainedLS : 1.0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeTex);
	glUniform1i(cubeTexLoc, 0);

	glDrawArrays(GL_TRIANGLES, 0, 36);
	glDepthMask(GL_TRUE);
	view = lookAt(camera.position, camera.viewVec + camera.position, camera.upVec);
}

void renderGrass() {
	glBindVertexArray(quadVAO);
	glUseProgram(grass.program);
	glUniformMatrix4fv(grass.mvpLoc, 1, GL_FALSE, value_ptr(proj * view));
	glUniformMatrix4fv(grass.shadowMatLoc, 1, GL_FALSE, value_ptr(shadowSbpv));
	glUniform1f(grass.moveFactorLoc, grass.moveFactor);
	glUniform1f(grass.lightStrLoc, rainflag ? rainedLS : 1.0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, grass.dfTex);
	glUniform1i(grass.dfTexLoc, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, terrain.heightMap);
	glUniform1i(grass.heightMapLoc, 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, grass.noiseTex);
	glUniform1i(grass.noiseTexLoc, 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, shadowDepTex);
	glUniform1i(grass.shadowTexLoc, 3);

	glDrawArraysInstanced(GL_TRIANGLES, 0, 12, 512 * 512);
}

void renderRain() {
	// Draw particles using updated buffers using additive blending.
	glBindVertexArray(rain.vao);
	glUseProgram(rain.renderProgram);
	glUniformMatrix4fv(rain.mvLoc, 1, GL_FALSE, value_ptr(view));
	glUniformMatrix4fv(rain.pLoc, 1, GL_FALSE, value_ptr(proj));
	glUniform3fv(rain.velocityLoc, 1, value_ptr(rain.velocity));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, grass.noiseTex);
	glUniform1i(rain.noiseLoc, 0);

	if (rain.velocity.y < -350.0f) {
		rain.velocity.y += 145.5;
	}
	else
		rain.velocity -= vec3(0.0, 6.0, 0.0);
	//cout << rain.velocity.y << endl;
	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 6, 64 * 64);
}

void renderCameraLine() {
	glUseProgram(cameraLine.program);

	glBindVertexArray(cameraLine.VAO);
	glUniformMatrix4fv(cameraLine.mvpLoc, 1, GL_FALSE, value_ptr(proj * view));

	glDrawArrays(GL_LINE_STRIP, 0, (cameraLine.numPts / 3) + 1);
}

void renderCameraDrawedLine() {
	glUseProgram(cameraLine.program);

	glBindVertexArray(cameraLine.vaoLine);
	glUniformMatrix4fv(cameraLine.mvpLoc, 1, GL_FALSE, value_ptr(proj * view));

	glDrawArrays(GL_LINE_STRIP, 0, 31);
}

void My_Display()
{
	float p[] = { ptPos.x, ptPos.y, ptPos.z };
	glBindVertexArray(cameraLine.VAO);
	glBindBuffer(GL_ARRAY_BUFFER, cameraLine.VBO);
	glBufferSubData(GL_ARRAY_BUFFER, cameraLine.numPts * sizeof(float), 3 * sizeof(float), (GLvoid*)p);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glViewport(0, 0, windowWidth, windowHeight);

	shadowBias = translate(mat4(1.0), vec3(0.5f)) * scale(mat4(1.0), vec3(0.5f));
	shadowSbpv = shadowBias * lightProj * lightView;

	water.renderRFR_RFL();
	renderShadowMap();

	//first color pass
	glBindFramebuffer(GL_FRAMEBUFFER, window.FBO);
	GLenum drawBuffers2[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
	glDrawBuffers(2, drawBuffers2);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderCubeBox();
	renderCameraLine();
	renderTerrain(vec4(0.0));
	renderWater();
	renderModels(vec4(0.0));
	renderGrass();
	renderSword();
	renderSphere();
	if(rainflag)
		renderRain();

	glDrawBuffer(GL_COLOR_ATTACHMENT3);
	glClear(GL_COLOR_BUFFER_BIT);
	glDepthMask(GL_FALSE);
	SSAOpass();
	glDepthMask(GL_TRUE);

	if (effect == 1)
	{
		//blur pass
		glDrawBuffer(GL_COLOR_ATTACHMENT2);
		glClear(GL_COLOR_BUFFER_BIT);
		glDepthMask(GL_FALSE);
		renderBlurTexture(window.texFBO);
		glDepthMask(GL_TRUE);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		renderDepFieldTexture();
	}
	else {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		renderWaterColorTexture();
	}

	glUseProgram(0);
	glBindVertexArray(0);
	glutSwapBuffers();
}

void My_Reshape(int width, int height)
{
	windowWidth = width;
	windowHeight = height;
	glViewport(0, 0, width, height);
	float aspect = (float)width / (float)height;
	proj = perspective(radians(80.0f), aspect, 0.1f, 300.0f);
	view = lookAt(camera.position, camera.viewVec + camera.position, camera.upVec);
	reshapeDepthFBO();
	water.reshape(width, height);
	reshapeWindowFBO(width, height);

}

void My_Timer(int val)
{
	timer_cnt += 0.1;
	glutPostRedisplay();
	if (timer_enabled)glutTimerFunc(timer_speed, My_Timer, val);
}

void bz() {
	static int start_idx = 0;
	float t = camera.moveFactor;
	vec3 in[4], vc[4];
	for (int i = 0; i < 4; i++) {
		in[i] = pts[start_idx + i];
		vc[i] = viewCenterPts[start_idx + i];
	}

	camera.position =
		in[0] * pow((1 - t), 3) + 3.0f * in[1] * t * pow((1 - t), 2) +
		3.0f * in[2] * pow(t, 2) * (1 - t) + in[3] * pow(t, 3);

	camera.viewVec =
		(vc[0] * pow((1 - t), 3) + 3.0f * vc[1] * t * pow((1 - t), 2) +
		3.0f * vc[2] * pow(t, 2) * (1 - t) + vc[3] * pow(t, 3)) - camera.position;

	if (camera.moveFactor >= 1.0 - camera.moveSpeed) {
		if (start_idx == 27) {
			camera.moveSpeed = 0.0;
		}
		else {
			start_idx += 3;
		}
	}
}

void self_Timer(int val)
{
	if (camera.moveSpeed != 0) {
		bz();
	}
	water.moveFactor += 0.0005;
	water.moveFactor = fmod(water.moveFactor, 1.0);
	grass.moveFactor += 0.05;
	grass.moveFactor = fmod(grass.moveFactor, 2 * 3.14159);
	glutPostRedisplay();
	glutTimerFunc(timer_speed, self_Timer, val);
	camera.moveFactor += camera.moveSpeed;
	camera.moveFactor = fmod(camera.moveFactor, 1.0);
}

void My_Mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			mouseDrag = true;
			off_x = x, off_y = y;
		}
		else if (state == GLUT_UP)
		{
			mouseDrag = false;
		}
	}
	if (button == 4) {
		camera.moveSpeed = camera.moveSpeed > 0.00001 ? camera.moveSpeed - 0.0001 : 0.00001;
	}
	if (button == 3) {
		camera.moveSpeed = camera.moveSpeed + 0.0001;
	}
}

void My_Cursor(int x, int y) {
	if (mouseDrag) {
		double degX, degY;
		degX = -1 * (x - off_x) / 10.0;
		degY = -1 * (y - off_y) / 10.0;
		camera.viewVec = normalize(vec3(rotate(mat4(1.0), (float)radians(degX), vec3(0.0, 1.0, 0.0)) * vec4(camera.viewVec, 1.0)));
		camera.viewVec = normalize(vec3(rotate(mat4(1.0), (float)radians(degY), cross(camera.viewVec, camera.upVec)) * vec4(camera.viewVec, 1.0)));
		view = lookAt(camera.position, camera.viewVec + camera.position, camera.upVec);
	}
	off_x = x, off_y = y;
}

void My_Keyboard(unsigned char key, int x, int y)
{
	//printf("Key %c is pressed at (%d, %d)\n", key, x, y);
	if (key == 'w' || key == 'W') {
		camera.position += normalize(vec3(camera.viewVec[0], 0.0, camera.viewVec[2]));
	}
	if (key == 's' || key == 'S') {
		camera.position -= normalize(vec3(camera.viewVec[0], 0.0, camera.viewVec[2]));
	}
	if (key == 'a' || key == 'A') {
		camera.position += normalize(cross(camera.upVec, camera.viewVec));
	}
	if (key == 'd' || key == 'D') {
		camera.position -= normalize(cross(camera.upVec, camera.viewVec));
	}
	if (key == 'i' || key == 'I') {
		ptPos += normalize(vec3(camera.viewVec[0], 0.0, camera.viewVec[2]));
	}
	if (key == 'k' || key == 'K') {
		ptPos -= normalize(vec3(camera.viewVec[0], 0.0, camera.viewVec[2]));
	}
	if (key == 'J' || key == 'j') {
		ptPos += normalize(cross(camera.upVec, camera.viewVec));
	}
	if (key == 'l' || key == 'L') {
		ptPos -= normalize(cross(camera.upVec, camera.viewVec));
	}
	if (key == 'y' || key == 'Y') {
		ptPos += vec3(0.0, 0.3, 0.0);
	}
	if (key == 'h' || key == 'H') {
		ptPos -= vec3(0.0, 0.3, 0.0);
	}
	if (key == 'z' || key == 'Z') {
		camera.position += vec3(0.0, 0.5, 0.0);
	}
	if (key == 'x' || key == 'X') {
		camera.position -= vec3(0.0, 0.5, 0.0);
	}
	if (key == 'q' || key == 'Q') {
		models[0].r += vec3(0.0, 0.1, 0.0);
	}
	if (key == 'e' || key == 'E') {
		models[0].r -= vec3(0.0, 0.1, 0.0);
	}
	if (key == 'R' || key == 'r') {
		if (rainflag)
			rainflag = 0;
		else
			rainflag = 1;
	}
	if (key == 'o' || key == 'O') {
		ssaoFlag = !ssaoFlag;
	}
	if (key == 'p' || key == 'P') {
		cout <<"vec3(" << ptPos.x << ", " << ptPos.y << ", " << ptPos.z << "),\n";
		float p[] = { ptPos.x, ptPos.y, ptPos.z };
		glBindVertexArray(cameraLine.VAO);
		glBindBuffer(GL_ARRAY_BUFFER, cameraLine.VBO);
		glBufferSubData(GL_ARRAY_BUFFER, cameraLine.numPts * sizeof(float), 3 * sizeof(float), (GLvoid*)p);
		cameraLine.numPts += 3;
	}
	if (key == 't' || key == 'T') {
		cameraLine.numPts = cameraLine.numPts == 3 ? 3 : cameraLine.numPts - 3;
		cout << "delete pt\n";
	}
	view = lookAt(camera.position, camera.viewVec + camera.position, camera.upVec);
}

void My_SpecialKeys(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_PAGE_UP:
		printf("Page up is pressed at (%d, %d)\n", x, y);
		break;
	case GLUT_KEY_LEFT:
		break;
	case GLUT_KEY_RIGHT:
		break;
	default:
		printf("Other special key is pressed at (%d, %d)\n", x, y);
		break;
	}
}

void My_Menu(int id)
{
	switch (id)
	{
	case MENU_TIMER_START:
		if (!timer_enabled)
		{
			timer_enabled = true;
			glutTimerFunc(timer_speed, My_Timer, 0);
		}
		break;
	case MENU_TIMER_STOP:
		timer_enabled = false;
		break;
	case MENU_EXIT:
		exit(0);
		break;
	case MENU_SHOW_DEPTH_OF_VIEW:
		effect = 1;
		break;
	case MENU_SHOW_WATERCOLOR:
		effect = 2;
		break;
	case MENU_NO_EFFECT:
		effect = 0;
		break;
	default:
		break;
	}
}

int main(int argc, char *argv[])
{
#ifdef __APPLE__
	// Change working directory to source code path
	chdir(__FILEPATH__("/../Assets/"));
#endif
	// Initialize GLUT and GLEW, then create a window.
	////////////////////
	glutInit(&argc, argv);
#ifdef _MSC_VER
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#else
	glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutInitContextVersion(4, 2);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(1600, 900);
	glutCreateWindow("AS2_Framework"); // You cannot use OpenGL functions before this line; The OpenGL context must be created first by glutCreateWindow()!
#ifdef _MSC_VER
	glewExperimental = GL_TRUE;
	glewInit();
#endif
	dumpInfo();
	My_Init();

	// Create a menu and bind it to mouse right button.
	int menu_main = glutCreateMenu(My_Menu);
	int menu_timer = glutCreateMenu(My_Menu);
	int menu_effect = glutCreateMenu(My_Menu);

	glutSetMenu(menu_main);
	glutAddSubMenu("Timer", menu_timer);
	glutAddSubMenu("Effect", menu_effect);
	glutAddMenuEntry("Exit", MENU_EXIT);

	glutSetMenu(menu_timer);
	glutAddMenuEntry("Start", MENU_TIMER_START);
	glutAddMenuEntry("Stop", MENU_TIMER_STOP);

	glutSetMenu(menu_effect);
	glutAddMenuEntry("depth_of_view", MENU_SHOW_DEPTH_OF_VIEW);
	glutAddMenuEntry("water_color", MENU_SHOW_WATERCOLOR);
	glutAddMenuEntry("original", MENU_NO_EFFECT);

	glutSetMenu(menu_main);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// Register GLUT callback functions.
	glutDisplayFunc(My_Display);
	glutReshapeFunc(My_Reshape);
	glutMouseFunc(My_Mouse);
	glutKeyboardFunc(My_Keyboard);
	glutSpecialFunc(My_SpecialKeys);
	glutTimerFunc(timer_speed, My_Timer, 0);
	glutTimerFunc(timer_speed, self_Timer, 0);
	glutMotionFunc(My_Cursor);

	// Enter main event loop.
	glutMainLoop();

	return 0;
}
