#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

uniform sampler2D normalMap;
uniform sampler2D landTex;
uniform sampler2D dudvMap;
uniform sampler2D waterNormal;
uniform sampler2DShadow shadowTex;
uniform float lightStr;

uniform float moveFactor;

in TES_OUT{
	vec2 tc;
	vec4 shadowTc;
	vec3 lightVec;
	vec3 world_coord;
	vec3 view_coord;
	vec3 waterDis;
}fs_in;

void main()
{
	vec3 normal = normalize(texture(normalMap, fs_in.tc).rgb);
	normal = vec3(normal.g, normal.b, normal.r) * 2.0 - 1.0;
	vec3 Intensity = vec3(0.0, 0.0, 0.0);

	vec2 distor1 = texture(dudvMap, vec2(fs_in.tc.x + moveFactor, fs_in.tc.y)).rg * 0.1;
	distor1 = fs_in.tc + vec2(distor1.x, distor1.y + moveFactor);
	vec3 shine = texture(waterNormal, distor1).rgb * 2.0 - 1.0;

	float wet = max(0.0, pow(1.0 / (fs_in.world_coord.y + 1.2), 16));
	wet = clamp(wet, 0.0, 0.5) * 0.5;

	float cWater, cWaterSp, cWaterGlow, cWaterView;
	cWater = dot(normalize(vec3(0.2, 0.2, 1.0)), normalize(shine));
	cWaterSp = max(0.0, pow(cWater, 128)) * 12.0;
	cWaterGlow = max(0.0, pow(cWater, 16)) * 4.0;
	cWaterView = clamp(length(fs_in.view_coord / 8.0), 0.0, 1.0);

	vec3 colorWaterGlow = cWaterGlow * vec3(0.6, 0.9, 1.0) * 0.2;
	vec3 colorWaterSp = vec3(cWaterSp);
	vec3 colorWaterView = vec3(0.13, 0.44, 1.0);

	float shadowFactor = textureProj(shadowTex, fs_in.shadowTc);
	shadowFactor = clamp(shadowFactor, 0.3, 1.0);

	float cKd, cKs;

	//ambient
	Intensity += texture(landTex, fs_in.tc).rgb * vec3(0.2, 0.2, 0.18) * 1.5;

	//diffuse
	cKd = max(0.0, dot(normal, normalize(fs_in.lightVec)));
	Intensity += lightStr * cKd * texture(landTex, fs_in.tc).rgb * (1.4 + cWaterSp * wet) + colorWaterGlow * wet;

	Intensity = mix(Intensity, colorWaterView, wet * cWaterView);

	normalColor = vec4(normal * 0.5 + 0.5, 1.0);
	fragColor = vec4(Intensity * shadowFactor + wet * vec3(0.0, 0.3, 0.5), 1.0);
}