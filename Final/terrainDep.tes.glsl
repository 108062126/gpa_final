#version 410
layout (quads) in;

uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform vec4 plane;

uniform sampler2D heightMap;

in TCS_OUT{
	vec2 tc;
}tcs_in[];

void main(){
	vec2 tc1 = mix(tcs_in[0].tc, tcs_in[1].tc, gl_TessCoord.x);
	vec2 tc2 = mix(tcs_in[2].tc, tcs_in[3].tc, gl_TessCoord.x);
	vec2 tc = mix(tc1, tc2, gl_TessCoord.y);

	vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
	vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
	vec4 p = mix(p1, p2, gl_TessCoord.y);
	p.y += texture(heightMap, tc).r * 6.0 - 2.0;

	vec4 pos = um4m * p;

	gl_ClipDistance[0] = dot(pos, plane);

	gl_Position = um4p * um4v * pos;
}