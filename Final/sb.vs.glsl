#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec3 iv3normal;

uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform mat4 shadowMatrix;

out VertexData
{
    vec3 N; // eye space normal
    vec3 L; // eye space light vector
    vec3 H; // eye space halfway vector
	vec3 V;
	vec4 shadowCoord;
} vertexData;

void main()
{
	vec4 position = um4m * vec4(iv3vertex, 1.0);
	vertexData.N = vec3(um4v * transpose(inverse(um4m)) * vec4(iv3normal, 0.0));
	vertexData.L = (um4v * vec4(-31.75, 26.05, -97.72, 0.0)).xyz;
	vertexData.V = -1 * (um4v * position).xyz;
	vertexData.H = normalize(vertexData.L) + normalize(vertexData.V);
	vertexData.shadowCoord = shadowMatrix * position;

	gl_Position = um4p * um4v * position;
}