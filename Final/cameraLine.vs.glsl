#version 410

layout(location = 0) in vec3 iv3vertex;

uniform mat4 mvp;

void main()
{
	gl_Position = mvp * vec4(iv3vertex, 1.0);
}