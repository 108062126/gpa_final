#version 410

layout(location = 0) in vec3 iv3texDir;

out vec3 texDir;

uniform mat4 um4mv;
uniform mat4 um4p;

void main(){

	texDir = iv3texDir;
	gl_Position = um4p * um4mv * vec4(iv3texDir, 1.0);

}