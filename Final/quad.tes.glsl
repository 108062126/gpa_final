#version 410
layout (quads) in;

uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform mat4 shadowMat;
uniform vec3 camPos;
uniform sampler2D heightMap;
uniform vec4 plane;


in TCS_OUT{
	vec2 tc;
}tcs_in[];

out TES_OUT{
	vec2 tc;
	vec4 shadowTc;
	vec3 lightVec;
	vec3 world_coord;
	vec3 view_coord;
	vec3 waterDis;
}tes_out;

void main(){
	vec2 tc1 = mix(tcs_in[0].tc, tcs_in[1].tc, gl_TessCoord.x);
	vec2 tc2 = mix(tcs_in[2].tc, tcs_in[3].tc, gl_TessCoord.x);
	vec2 tc = mix(tc1, tc2, gl_TessCoord.y);

	vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
	vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
	vec4 p = mix(p1, p2, gl_TessCoord.y);
	p.y += texture(heightMap, tc).r * 6.0 - 2.0;

	vec4 pos = um4m * p;

	gl_ClipDistance[0] = dot(pos, plane);

	tes_out.tc = tc;
	tes_out.world_coord = pos.xyz;
	tes_out.view_coord = (um4v * pos).xyz;
	tes_out.shadowTc = shadowMat * pos;
	tes_out.lightVec = vec3(-4.0, 5.5, 2.5);
	gl_Position = um4p * um4v * pos;
}