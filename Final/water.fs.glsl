#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

uniform sampler2D texRFR;
uniform sampler2D texRFL;
uniform sampler2D dudvMap;
uniform sampler2D normalMap;
uniform sampler2DShadow shadowTex;
uniform float lightStr;

uniform float moveFactor;

in TES_OUT{
	vec3 viewVec;
	vec3 lightVec;
	vec2 dudvTc;
	vec4 texCoord;
	vec4 shadowTc;
}fs_in;

void main(){
	vec2 ndc = (fs_in.texCoord.xy / fs_in.texCoord.w) / 2.0 + 0.5;

	vec2 distor1 = texture(dudvMap, vec2(fs_in.dudvTc.x + moveFactor, fs_in.dudvTc.y)).rg * 0.1;
	distor1 = fs_in.dudvTc + vec2(distor1.x, distor1.y + moveFactor);
	vec2 distor = (texture(dudvMap, distor1).rg * 2.0 - 1.0) * 0.02;

	ndc += distor;
	ndc = clamp(ndc, 0.001, 0.999);
	vec4 rflColor = texture(texRFL, vec2(1.0 - ndc.x, ndc.y));
	vec4 rfrColor = texture(texRFR, vec2(ndc.x, ndc.y));

	float shadowFactor = textureProj(shadowTex, fs_in.shadowTc);
	shadowFactor = clamp(shadowFactor, 0.4, 1.0);

	vec3 normal = texture(normalMap, distor1).rgb;
	normal = vec3(normal.r * 2.0 - 1.0, normal.b, normal.g * 2.0 - 1.0);
	normal = normalize(normal);


	vec3 HV = normalize(normalize(fs_in.lightVec) + normalize(fs_in.viewVec));
	float sp = max(0, pow(dot(normal, normalize(HV)), 128));
	sp = clamp(sp, 0.0, 1.0);

	normalColor = vec4(vec3(normal.r, normal.b, normal.g) * 0.5 + 0.5, 1.0);
	fragColor = (mix(mix(rflColor, rfrColor, dot(normalize(fs_in.viewVec), normal)), vec4(0.0, 0.3, 0.5, 1.0), 0.1) + vec4(sp * 0.7, sp * 0.8, sp * 0.9, 0.0)) * vec4(vec3(shadowFactor), 1.0);
}