#version 410

out vec2 texcoord;

void main()
{
	vec4 positions[4] = vec4[](
		vec4(-1.0, -1.0, 0.0, 1.0),
		vec4(1.0, -1.0, 0.0, 1.0),
		vec4(-1.0, 1.0, 0.0, 1.0),
		vec4(1.0, 1.0, 0.0, 1.0)
	);

	gl_Position = positions[gl_VertexID];
	texcoord = positions[gl_VertexID].xy * 0.5 + 0.5;
}