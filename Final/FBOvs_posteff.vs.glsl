#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec2 iv2texcoord;

out vec2 texcoord;

uniform mat4 ortho;

void main(){
	gl_Position = ortho * vec4(iv3vertex.xy, 0.0, 1.0);
	texcoord = iv2texcoord;
}