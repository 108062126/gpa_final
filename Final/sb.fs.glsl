#version 410

layout(location = 0) out vec4 fragColor;

in VertexData
{
    vec3 N; // eye space normal
    vec3 L; // eye space light vector
    vec3 H; // eye space halfway vector
	vec3 V;
	vec4 shadowCoord;
} vertexData;

uniform samplerCube cubemap;
uniform sampler2DShadow shadowTex;

void main()
{
	float s = textureProj(shadowTex, vertexData.shadowCoord);
	vec3 Intensity = vec3(0.0, 0.0, 0.0);
	float cKd, cKs, d, attenuation;
	vec3 envColor = texture(cubemap, reflect(normalize(vertexData.V), normalize(vertexData.N))).rgb;

	d = distance(vertexData.L, vec3(0.0, 0.0, 0.0));
	attenuation = min(1/(0.01 + d*0.000003 + d*d*0.00001), 1);

	//ambient
	//Intensity += vec3(0.2, 0.2, 0.1985);

	//diffuse
	cKd = max(0, dot(normalize(vertexData.L), normalize(vertexData.N)));
	Intensity += cKd * 0.35;

	//specular
	cKs = max(0, pow(dot(normalize(vertexData.H), normalize(vertexData.N)), 200));
	Intensity += cKs * 0.7;

	s = clamp(s, 0.2, 1.0);
	fragColor = vec4(mix(Intensity, envColor, 0.35), 1.0) * s;
}