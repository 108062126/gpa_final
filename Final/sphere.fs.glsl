#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

uniform samplerCube texCube;

in VertexData
{
    vec3 N; 
    vec3 view;
    vec2 texcoord;
} fs_in;


void main()
{
	vec3 normal = normalize(fs_in.N);
    vec3 r = reflect(normalize(fs_in.view), normal);
    vec3 envColor = texture(texCube, r).rgb;    
    
    fragColor = vec4(envColor, 1.0) * vec4(vec3(0.7), 1.0);  
	normalColor = vec4(normal * 0.5 + 0.5, 1.0);

}