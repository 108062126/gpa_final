#version 410

layout(location = 0) out vec4 fragColor;
layout(location = 1) out vec4 normalColor;

uniform samplerCube texCube;
uniform sampler2D Sting_Base_Color;
uniform sampler2DShadow shadowTex;

in VertexData
{
    vec3 N2; 
    vec3 L;
    vec3 V;
    vec3 H;

    vec3 N; 
    vec3 view;
    vec2 texcoord;
    vec4 shadowcoord;
} fs_in;



void main()
{
	vec3 normal = normalize(fs_in.N);
    // vec3 r = reflect(normalize(fs_in.view), normalize(fs_in.N));
    
    vec3 texColor = texture(Sting_Base_Color, fs_in.texcoord).rgb;
    float shadowFactor = textureProj(shadowTex, fs_in.shadowcoord);
	shadowFactor = clamp(shadowFactor, 0.3, 1.0);
    
    /*
    vec3 ambient = texColor * vec3(0.3f);
    vec3 diffuse = max(dot(fs_in.N, fs_in.L), 0.0) * texColor * vec3(0.5f);
    vec3 specular = pow(max(dot(fs_in.N, fs_in.H), 0.0), 200.0) * vec3(0.7);
    */

    //vec3 diffuse = max(dot(fs_in.N, fs_in.L), 0.0) * texColor;
    //vec3 specular = pow(max(dot(fs_in.N, fs_in.H), 0.0), 200.0) * vec3(0.2);

    //fragColor = vec4(pow(ambient + diffuse + specular, vec3(1.0/2.2)), 1.0);


    fragColor = vec4(texColor * shadowFactor, 1.0);
	normalColor = vec4(normal * 0.5 + 0.5, 1.0);
}