#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec2 iv2tex_coord;

uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;

uniform vec4 plane;

out vec2 texCoord;

void main()
{
	vec4 pos = um4m * vec4(iv3vertex, 1.0);
	texCoord = iv2tex_coord;
	gl_ClipDistance[0] = dot(pos, plane);
	gl_Position = um4p * um4v * pos;
}