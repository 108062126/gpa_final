#version 410

uniform mat4 mvp;
uniform mat4 shadowMat;
uniform sampler2D noiseMap;
uniform sampler2D heightMap;
uniform float moveFactor;

out vec2 texCoord;
out vec4 shadowTc;
out vec3 fragNormal;
out vec2 tc;

void main(){
	const vec4 vertices[] = vec4[](
		vec4(-0.25,  0.25, 0.0, 1.0),
		vec4(-0.25, -0.25, 0.0, 1.0),
		vec4(0.25, -0.25, 0.0, 1.0),
		vec4(0.25, -0.25, 0.0, 1.0),
		vec4(0.25,  0.25, 0.0, 1.0),
		vec4(-0.25,  0.25, 0.0, 1.0),

		vec4(0.0, -0.25, 0.25, 1.0),
		vec4(0.0, 0.25, 0.25, 1.0),
		vec4(0.0, -0.25, -0.25, 1.0),
		vec4(0.0, -0.25, -0.25, 1.0),
		vec4(0.0, 0.25, -0.25, 1.0),
		vec4(0.0, 0.25, 0.25, 1.0)
	);

	int x = gl_InstanceID & 511;
	int y = gl_InstanceID >> 9;
	vec2 offset = vec2(x, y);

	float noise;
	vec4 normal;
	mat4 rotation;
	vec2 distor;
	vec4 dis = vec4(0.0, 0.0, 0.0, 0.0);

	tc = (vertices[gl_VertexID].xz + offset + vec2(4.0)) / 512.0;
	noise = texture(noiseMap, tc).r;
	distor = texture(noiseMap, vec2(tc.x, tc.y)).rg;
	distor = distor + texture(noiseMap, vec2(tc.x, tc.y + distor.y)).rg;
	distor = distor * 2.0 - 1.0;

	noise *= 5.0;
	rotation = mat4(
		vec4(cos(noise), 0.0, sin(noise), 0.0),
		vec4(0.0, 1.0, 0.0, 0.0),
		vec4(-sin(noise), 0.0, cos(noise), 0.0),
		vec4(0.0, 0.0, 0.0, 1.0)
	);

	if(gl_VertexID <6){
		normal = rotation * vec4(0.0, 0.0, 1.0, 0.0);
		texCoord = (vertices[gl_VertexID].xy * 4.0) * 0.5 + 0.5;

		if(vertices[gl_VertexID].y > 0)
			dis = sin(moveFactor + noise) * normal * 0.1;
	}
	else {
		normal = rotation * vec4(1.0, 0.0, 0.0, 0.0);
		texCoord = (vertices[gl_VertexID].zy * 2.0) + 0.5;

		if(vertices[gl_VertexID].y > 0)
			dis = cos(moveFactor + noise) * normal * 0.1;
	}

	vec4 pos = (rotation * vertices[gl_VertexID]) + dis + vec4(distor.x, 0.0, distor.y, 0.0);
	tc = (pos.xz + offset + distor + vec2(8.0)) / 512.0;
	pos.y += texture(heightMap, tc).r * 6.0 - 2.2;
	shadowTc = shadowMat * (pos + vec4(float(offset.x - 256), 0.0, float(offset.y - 256), 0.0) / 8.0);
	fragNormal = -normal.rgb;
	gl_Position = mvp * (pos + vec4(float(offset.x - 256), 0.0, float(offset.y - 256), 0.0) / 8.0);
}