#version 410
layout (quads) in;

uniform mat4 um4v;
uniform mat4 um4p;
uniform mat4 shadowMat;
uniform vec3 camPos;

uniform sampler2D disMap;
uniform sampler2D dudvMap;
uniform float moveFactor;

in TCS_OUT{
	vec2 tc;
}tcs_in[];

out TES_OUT{
	vec3 viewVec;
	vec3 lightVec;
	vec2 dudvTc;
	vec4 texCoord;
	vec4 shadowTc;
}tes_out;

void main(){

	vec2 tc1 = mix(tcs_in[0].tc, tcs_in[1].tc, gl_TessCoord.x);
	vec2 tc2 = mix(tcs_in[2].tc, tcs_in[3].tc, gl_TessCoord.x);
	vec2 tc = mix(tc1, tc2, gl_TessCoord.y);

	vec2 distor1 = texture(dudvMap, vec2(tc.x + moveFactor, tc.y)).rg * 0.1;
	distor1 = tc + vec2(distor1.x, distor1.y + moveFactor);
	float displacement = texture(disMap, distor1).r * 1.5;

	vec4 p1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
	vec4 p2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
	vec4 p = mix(p1, p2, gl_TessCoord.y);
	p.y += displacement - 1.0;

	tes_out.viewVec = camPos - p.xyz;
	tes_out.lightVec = vec3(-4.0, 5.5, 2.5);
	tes_out.dudvTc = tc;
	tes_out.texCoord = um4p * um4v * p;
	tes_out.shadowTc = shadowMat * p;
	gl_Position = tes_out.texCoord;
}