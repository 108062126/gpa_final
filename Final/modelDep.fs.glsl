#version 410

layout(location = 0) out vec4 fragColor;

uniform sampler2D tex;

in vec2 texCoord;

void main()
{
	if(texture(tex, texCoord).a < 0.1)discard;
	fragColor = vec4(vec3(gl_FragCoord.z), 1.0);
}