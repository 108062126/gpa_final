#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec3 iv3normal;

out VS_OUT{
	vec2 tc;
}vs_out;

void main()
{
	int x = gl_InstanceID & 63;
	int y = gl_InstanceID >> 6;
	vec2 offset = vec2(x, y);
	vs_out.tc = (iv3vertex.xz + offset + vec2(0.5)) / 64.0;
	gl_Position = vec4(iv3vertex, 1.0) + vec4(float(offset.x - 32), 0.0, float(offset.y - 32), 0.0);
}