#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 1) in vec2 iv2tex_coord;
layout(location = 2) in vec3 iv3normal;

uniform mat4 um4m;
uniform mat4 trans_inv_m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform vec3 camera_pos;

out VertexData
{
    vec3 N; 
    vec3 view;
    vec2 texcoord;
} vertexData;



void main()
{
    vertexData.texcoord = iv2tex_coord;

    vec4 pos = um4m * vec4(iv3vertex, 1.0);
    vertexData.view = pos.xyz - camera_pos;
    vertexData.N = mat3(trans_inv_m) * iv3normal;
	gl_Position = um4p * um4v * pos;
}